class AppTechu extends Polymer.Element {
  static get is() { return 'app-techu'; }
  static get properties() {
    return {
      login: {
        type: Boolean,
        notify: true,
      },
      checkCtas: {
        type: Boolean,
        notify: true,
      },
      checkMovtos: {
        type: Boolean,
        notify: true,
      }
    };
  }

  onload(){
    this.set('login', true);
    this.set('checkCtas', false);
    this.set('isOK', false);
  }
}
window.customElements.define(AppTechu.is, AppTechu);
