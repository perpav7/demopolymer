class AltaMovimientos extends Polymer.Element {
  static get is() { return 'alta-movimientos'; }
  static get properties() {
    return {
      nombreCliente: {
        type: String,
        notify: true,
      },
      datosCta: {
        type: Object,
        notify: true,
      },
      aCtas:{
        type: Array,
        notify: true,
      },
    };
  }
  ready(){
    super.ready();
    this.set('detalleCtas',sessionStorage.getItem("detalleCtas"));
    this.set('numCliente',sessionStorage.getItem("numCliente"));
    this.set('aliasCta',sessionStorage.getItem("aliasCta"));
    this.set('numCta',sessionStorage.getItem("numCta"));
    var datetime = new Date();
    var fMes = datetime.getMonth()+1;
    var fechacargo = datetime.getFullYear() + "-" + fMes + "-" + datetime.getDate();
    this.set('fechaopera',fechacargo);
    this.aCtas = new Array;
    var detMovtos = JSON.parse(this.detalleCtas);
  }

  transferir(ev){
    ev.preventDefault();
    console.log(ev.target);
    if (this.descripcion != "" && this.cuentadestino != "" && this.importe != "") {
        var paramsTransfer = {'cliente': this.numCliente,'numCuenta': this.numCta,'cargo': this.importe,'fechaCargo': this.fechaopera,'descripcion': this.descripcion};
        console.log(paramsTransfer);
        var reqTransfer = new XMLHttpRequest();
        reqTransfer.open("POST", "http://localhost:3000/altamovimientos", false);
        reqTransfer.setRequestHeader('Content-type','application/json; charset=utf-8');
        reqTransfer.send(JSON.stringify(paramsTransfer));
        this.resp = reqTransfer.responseText;
        if(this.resp.length == 0){
          console.log(this.resp);
        }else{
          alert("Operacion exitosa!!!");
          var desc = {'cliente': this.numCliente,'cuenta': this.numCta};
          var reqCons = new XMLHttpRequest();
          reqCons.open("POST", "http://localhost:3000/movimientos", false);
          reqCons.setRequestHeader('Content-type','application/json; charset=utf-8');
          reqCons.send(JSON.stringify(desc));
          this.detalleCtas = reqCons.responseText;
          location.href = "../../inicio.html";
          sessionStorage.setItem("detalleCtas",this.detalleCtas);
          sessionStorage.setItem("numCliente",this.cliente);
        }
    } else {
      alert("Los datos solicitados son obligatorios");
    }
  }
  regresa(){
    location.href = "../../movimientos.html";
    sessionStorage.setItem("detalleCtas",this.detalleCtas);
    sessionStorage.setItem("numCliente",this.numCliente);
  }
  salir(){
    sessionStorage.clear();
    alert("Gracias por utilizar la herramienta TechU");
    location.href = "../../index.html";
  }
}
window.customElements.define(AltaMovimientos.is, AltaMovimientos);
