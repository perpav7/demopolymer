class LoginForm extends Polymer.Element {
  static get is() { return 'login-form'; }
  static get properties() {
    return {
      username: {
        type: String,
        notify: true,
      },
      password: {
        type: String,
        notify: true,
      },
      isOK: {
        type: Boolean,
        notify: true,
      },
      nombreCliente: {
        type: String,
        notify: true,
      },
      numCliente: {
        type: String,
        notify: true,
      },
      datosCta: {
        type: Object,
        notify: true,
      },
    };
  }
  acceder(ev){
    ev.preventDefault();
    //if (this.disabledValidation || this.$.form.checkValidity()) {
    if (this.username != "" && this.password != "") {
        var paramsLogin = {'username': this.username, 'password': this.password};
        var reqLogin = new XMLHttpRequest();
        reqLogin.open("POST", "http://localhost:3000/login", false);
        reqLogin.setRequestHeader('Content-type','application/json; charset=utf-8');
        reqLogin.send(JSON.stringify(paramsLogin));
        this.login = reqLogin.responseText;
        if(this.login === "false"){
          this.isOK = false;
        }else{
          /*Llama a PG para carga de cuentas*/
          var a = JSON.parse(this.login);
          this.set('nombreCliente',a.nombreCliente);
          this.set('numCliente',a.numCliente);
          var paramsPG = {'numCliente': this.numCliente};
          var reqPG = new XMLHttpRequest();
          reqPG.open("POST", "http://localhost:3000/posicionglobal", false);
          reqPG.setRequestHeader('Content-type','application/json; charset=utf-8');
          reqPG.send(JSON.stringify(paramsPG));
          this.datosCta = reqPG.responseText;
          /**********************************/
          location.href = "../../inicio.html";
          sessionStorage.setItem("cliente",this.numCliente);
          sessionStorage.setItem("datosCta",this.datosCta);
          sessionStorage.setItem("nombreCliente",this.nombreCliente);
        }
    } else {
      alert("Los datos solicitados son obligatorios");
    }
  }
  ready(){
    this.set('username',sessionStorage.getItem("usuario"));
    this.set('password', '');
    this.set('isOK', true);
    this.set('nombreCliente', '');
    this.set('numCliente', '');
    super.ready();
  }
  registrar(){
    location.href = "../../registro.html";
  }
}
window.customElements.define(LoginForm.is, LoginForm);
