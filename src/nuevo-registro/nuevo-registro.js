class NuevoRegistro extends Polymer.Element {
  static get is() { return 'nuevo-registro'; }
  static get properties() {
    return {
      username: {
        type: String,
        notify: true,
      },
      password: {
        type: String,
        notify: true,
      },
      isOK: {
        type: Boolean,
        notify: true,
      },
      nombreCliente: {
        type: String,
        notify: true,
      },
      datosCta: {
        type: Object,
        notify: true,
      },
    };
  }
  ready(){
    super.ready();
    this.set('nombre', '');
    this.set('apellido', '');
    this.set('usuario', '');
    this.set('contrasena', '');
  }
  registrar(ev){
    ev.preventDefault();
    if (this.nombre != "" && this.apellido != "" && this.usuario != "" && this.contrasena != "") {
        var paramsLogin = {'nombre': this.nombre,'apellido': this.apellido,'idcliente': "A0000001",'usuario': this.usuario,'contrasena': this.contrasena}
        console.log(paramsLogin);
        var reqRegistro = new XMLHttpRequest();
        reqRegistro.open("POST", "http://localhost:3000/registrar", false);
        reqRegistro.setRequestHeader('Content-type','application/json; charset=utf-8');
        reqRegistro.send(JSON.stringify(paramsLogin));
        this.respReg = reqRegistro.responseText;
        console.log(this.respReg);
        if(this.respReg === "false"){
          alert("El usuario ya existe. Favor de ingresar un usuario diferente.");
          this.set('usuario', '');
          this.set('usuario', '');
          this.set('contrasena', '');
        }else{
          alert("Felicidades, tu registro ha sido exitoso!!!!")
          location.href = "../../index.html";
          sessionStorage.setItem("usuario",this.usuario);
        }
    } else {
      alert("Los datos solicitados son obligatorios");
    }
  }

  regresa(){
    location.href = "../../index.html";
    sessionStorage.setItem("usuario","");
  }

}
window.customElements.define(NuevoRegistro.is, NuevoRegistro);
