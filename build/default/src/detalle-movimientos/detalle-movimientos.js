class DetalleMovimientos extends Polymer.Element {
  static get is() {
    return 'detalle-movimientos';
  }

  static get properties() {
    return {
      nombreCliente: {
        type: String,
        notify: true
      },
      datosCta: {
        type: Object,
        notify: true
      },
      aCtas: {
        type: Array,
        notify: true
      }
    };
  }

  ready() {
    super.ready();
    this.set('detalleCtas', sessionStorage.getItem("detalleCtas"));
    this.set('numCliente', sessionStorage.getItem("numCliente"));
    this.set('aliasCta', sessionStorage.getItem("aliasCta"));
    this.aCtas = new Array();
    var detMovtos = JSON.parse(this.detalleCtas);

    for (var i = 0; i < detMovtos.length; i++) {
      if (detMovtos[i].cargo == "") {
        this.aCtas[i] = {
          date: detMovtos[i].fechaAbono,
          label: detMovtos[i].descripcion,
          category: "7",
          description: "Abono",
          parsedAmount: {
            value: detMovtos[i].abono,
            currency: "MXN"
          }
        };
      } else {
        this.aCtas[i] = {
          date: detMovtos[i].fechaCargo,
          label: detMovtos[i].descripcion,
          category: "7",
          description: "Cargo",
          parsedAmount: {
            value: "-" + detMovtos[i].cargo,
            currency: "MXN"
          }
        };
      }
    }

    var component2 = document.querySelector('#cells-navigation-bar-2');
    component2.items = [{
      label: 'cells-navigation-bar-home',
      id: 'home'
    }, {
      label: 'cells-navigation-bar-pay',
      id: 'assistance'
    }, {
      label: 'cells-navigation-bar-notifications',
      id: 'notifications'
    }];
  }

}

window.customElements.define(DetalleMovimientos.is, DetalleMovimientos);