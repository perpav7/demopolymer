class PosicionGlobal extends Polymer.Element {
  static get is() {
    return 'posicion-global';
  }

  static get properties() {
    return {
      username: {
        type: String,
        notify: true
      },
      password: {
        type: String,
        notify: true
      },
      isOK: {
        type: Boolean,
        notify: true
      },
      nombreCliente: {
        type: String,
        notify: true
      },
      datosCta: {
        type: Object,
        notify: true
      },
      aCtas: {
        type: Array,
        notify: true
      }
    };
  }

  ready() {
    super.ready();
    this.set('nombreCliente', sessionStorage.getItem("nombreCliente"));
    this.set('datosCta', sessionStorage.getItem("datosCta"));
    this.aCtas = new Array();
    var sCtas = JSON.parse(this.datosCta);

    for (var i = 0; i < sCtas.length; i++) {
      this.aCtas[i] = {
        nombre: sCtas[i].aliasCuenta,
        moneda: sCtas[i].moneda,
        descripcion: {
          value: sCtas[i].numCuenta,
          masked: true
        },
        detalle: {
          label: sCtas[i].tipoCuenta,
          amount: sCtas[i].saldoDisponible,
          currency: sCtas[i].moneda
        }
      };
    }
  }

  consultaDetalle(ev) {
    this.set('cliente', sessionStorage.getItem("cliente"));
    var desc = {
      'cliente': this.cliente,
      'cuenta': ev.target.__data.description.value
    };
    var reqCons = new XMLHttpRequest();
    reqCons.open("POST", "http://localhost:3000/movimientos", false);
    reqCons.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    reqCons.send(JSON.stringify(desc));
    this.detalleCtas = reqCons.responseText;

    if (this.detalleCtas === "null") {
      alert("La cuenta seleccionada no tiene movimientos");
    } else {
      location.href = "../../movimientos.html";
      sessionStorage.setItem("detalleCtas", this.detalleCtas);
      sessionStorage.setItem("numCliente", this.cliente);
      sessionStorage.setItem("aliasCta", ev.target.__data.name);
    }
  }

}

window.customElements.define(PosicionGlobal.is, PosicionGlobal);