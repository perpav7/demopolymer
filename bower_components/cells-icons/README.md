

##&lt;cells-icons&gt;

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

`cells-icons` is a utility import that includes the definition for the `atom-icon` element, `iron-iconset-svg` element, as well as an import for the default icon set.

The `cells-icons` directory also includes imports for additional icon sets that can be loaded into your project.

Example loading icon set:

```html
<link rel="import" href="../cells-icons/cells-icons.html">
```

To use an icon from one of these sets, first prefix your `iron-icon` with the icon set name, followed by a colon, ":", and then the icon id.

Example using the A01 icon from the buzz icon set:

```html
<iron-icon icon="cells:A01"></iron-icon>
```

See [iron-cells-iconset](#iron-cells-iconset) and [iron-iconset-svg](#iron-iconset-svg) for more information about how to create a custom cells-iconset.
