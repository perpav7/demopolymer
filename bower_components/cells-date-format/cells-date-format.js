class CellsDateFormat extends Polymer.Element {
  /* global moment */

  static get is() {
    return 'cells-date-format';
  }

  static get properties() {
    return {
      /**
       * Date formatter.
       *
       * @property format
       * @type {String}
       * @default 'YYYY-MM-DD'
       */
      format: {
        type: String,
        value: 'YYYY-MM-DD'
      },
      /**
       * Attribute that indicates if current year must be shown.
       * The year is not shown by default for dates of the current year.
       *
       * @property showCurrentYear
       * @type {Boolean}
       */
      showCurrentYear: {
        type: Boolean
      },
      /**
       * String that contains the date to operate with.
       *
       * @property date
       * @type {String}
       */
      date: {
        type: String
      },
      /**
       * Computed property that calculates parsed moment date based on current date.
       * Depends on date.
       *
       * @property momentData
       * @type {Object}
       */
      _momentData: {
        type: Object,
        computed: '_calculateMomentData(date)'
      },
      /**
       * Computed property that calculates datetime string for use on datetime tag.
       * Depends on momentData, format.
       *
       * @property datetime
       * @type {Object}
       */
      _datetime: {
        type: String,
        computed: '_calculateDatetime(_momentData, format)'
      },
      /**
       * Computed property that calculates data object through momentData, format, and showCurrentYear flag.
       * Depends on momentData, format, showCurrentYear.
       *
       * @property data
       * @type {Object}
       */
      _data: {
        type: Object,
        computed: '_calculateData(_momentData, format, showCurrentYear)'
      }
    };
  }

  /**
   * Calculates moment object data based on given date string.
   * It also fixes movement date when it matchs '00:00:00' format.
   *
   * @private
   * @method _calculateMomentData
   * @param  {String} date          String containing date to be formatted.
   * @return {Object}               Date parsed as moment object.
   */
  _calculateMomentData(date) {
    const ret = moment(date);

    if (date.match(/00:00:00/)) {
      return moment.parseZone(date);
    }

    return ret;
  }

  /**
   * Generates formatted datetime string based on given momentData and given format.
   *
   * @private
   * @method _calculateDatetime
   * @param  {Object} momentData  Data parsed as moment object.
   * @param  {String} format      Desired date format.
   * @return {String}             Formatted data string.
   */
  _calculateDatetime(momentData, format) {
    return momentData ? momentData.format(format) : '';
  }

  /**
   * Generates data object based on given moment data, format, and show current year flag.
   * It parses them and generates an object containing datetime, day, monthName, monthShort, and year properties.
   *
   * @private
   * @method  _calculateData
   * @param   {Object}  momentData        Data parsed as moment object.
   * @param   {String}  format            Desired format for calculate datetime.
   * @param   {Boolean} showCurrentYear   Flag that indicates of current year must be shown.
   * @return  {Object}
   */
  _calculateData(momentData, format, showCurrentYear) {
    if (momentData) {
      const year = momentData.year();
      const currentYear = moment().get('year');

      return {
        datetime: momentData.format(format),
        day: momentData.format('DD'),
        monthName: momentData.format('MMMM'),
        monthShort: momentData.format('MMM'),
        year: (!showCurrentYear && year === currentYear) ? null : year
      };
    }

    return {};
  }
}

customElements.define(CellsDateFormat.is, CellsDateFormat);
