![cells-date-format screenshot](cells-date-format.png)

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)

[Demo of component in Cells Catalog](https://au-bbva-ether-cellscatalogs.appspot.com/?view=docs#/component/cells-date-format)

# &lt;cells-date-format&gt;

`<cells-date-format>` displays a date with a predefined format using moment.
The year is not shown by default unless `showCurrentYear` is set to true.
For dates in the past, the year is shown.

The specified `format` is set as the `datetime` attribute of an internal [`<time>` element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time).

Example:

```html
<cells-date-format
  date="1988-10-01"
  format="DD-MM-YYYY">
</cells-date-format>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom Property                     | Description                             | Default                  |
| :---------------------------------- | :-------------------------------------- | :----------------------- |
| --cells-date-format-color           | text color                              | var(--bbva-500, #666666) |
| --cells-date-format                 | Mixin applied to :host                  | {}                       |
| --cells-date-format-time            | empty mixin applied to `<time>` element | {}                       |
| --cells-date-format-day-font-size   | font-size applied to the day            | 1.125rem                 |
| --cells-date-format-day             | empty mixin applied to day              | {}                       |
| --cells-date-format-month-font-size | font-size applied to the month          | 0.5625rem                |
| --cells-date-format-month           | empty mixin applied to the month        | {}                       |
| --cells-date-format-year-font-size  | font-size applied to the year           | 0.625rem                 |
| --cells-date-format-year            | empty mixin applied to the year         | {}                       |
