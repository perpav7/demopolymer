![cells-navigation-bar screenshot](cells-navigation-bar.png)

# cells-navigation-bar

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://bbva-ether-cellscatalogs.appspot.com/?view=demo#/component/cells-navigation-bar)

`<cells-navigation-bar>` displays an horizontal navigation bar with optional icons
for each navigation item. The placement of the icons can be customized using mixins.

Example:

```html
<cells-navigation-bar items="[[items]]" selected="0"></cells-navigation-bar>
```


 Examples of Menu items:
    [{
       label: 'translation-key',
       icon: 'icon-id',
       id: 'itemId',
       link: '#/target'
    }]

    [{
       label: 'translation-key-2',
       icon: 'icon-id2',
       id: 'itemId2',
       link: '#/target2',
       disabled: true
    }]


## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                              | Selector                   | CSS Property     | Value                                                             |
| -------------------------------------------- | -------------------------- | ---------------- | ----------------------------------------------------------------- |
| --cells-navigation-bar-height                | :host                      | height           |  3.125rem                                                         |
| --cells-navigation-bar-background-color      | :host                      | background-color |  rgba(255, 255, 255, 0.95)                                        |
| --cells-fontDefault                          | :host                      | font-family      |  sans-serif                                                       |
| --cells-navigation-bar-with-bump-list-color  | :host(.with-bump) .list    | background-color | --bbva-white                                                      |
| --cells-navigation-bar-background-color      | :host(.with-bump):before   | background-color |  rgba(255, 255, 255, 0.95)                                        |
| --cells-navigation-bar-height                | .list__item                | height           |  100%                                                             |
| --cells-navigation-bar-link-color            | .list__link                | color            |  ![#BDBDBD](https://placehold.it/15/BDBDBD/000000?text=+) #BDBDBD |
| --cells-navigation-bar-notification-color    | .list__link .notification  | color            |  white                                                            |
| --cells-navigation-bar-bg-notification-color | .list__link .notification  | background-color |  ![#b92a45](https://placehold.it/15/b92a45/000000?text=+) #b92a45 |
| --cells-navigation-bar-link-active-color     | .iron-selected .list__link | color            |  ![#004481](https://placehold.it/15/004481/000000?text=+) #004481 |
| --cells-navigation-bar-link-active-color     | .list__link:active         | color            |  ![#004481](https://placehold.it/15/004481/000000?text=+) #004481 |
| --cells-navigation-bar-link-active-color     | .list__link:focus          | color            |  ![#004481](https://placehold.it/15/004481/000000?text=+) #004481 |
### @apply
| Mixins                                | Selector                     | Value |
| ------------------------------------- | ---------------------------- | ----- |
| --cells-fontDefaultMedium             | :host                        | {}    |
| --cells-navigation-bar                | :host                        | {}    |
| --cells-navigation-bar-bump           | :host(.with-bump)            | {}    |
| --cells-navigation-bar-with-bump-list | :host(.with-bump) .list      | {}    |
| --cells-navigation-bar-bump-before    | :host(.with-bump):before     | {}    |
| --cells-navigation-bar-bump-after     | :host(.with-bump):after      | {}    |
| --cells-navigation-bar-list           | .list                        | {}    |
| --cells-navigation-bar-list-item      | .list__item                  | {}    |
| --cells-navigation-bar-link           | .list__link                  | {}    |
| --cells-navigation-bar-notification   | .list__link .notification    | {}    |
| --cells-navigation-bar-link-active    | .iron-selected .list__link   | {}    |
| --cells-navigation-bar-link-active    | .list__link:active           | {}    |
| --cells-navigation-bar-link-active    | .list__link:focus            | {}    |
| --cells-navigation-bar-link-disabled  | .list__link .disabled:active | {}    |
| --cells-navigation-bar-link-disabled  | .list__link .disabled:focus  | {}    |
| --cells-navigation-bar-icon           | .list__icon                  | {}    |


