
/**
 * @demo demo/index.html
* @summary displays an horizontal navigation bar with optional icons for each navigation item. The placement of the icons can be customized using mixins.
* @customElement
* @polymer
* @extends {Polymer.mixinBehaviors}
 * @appliesMixin CellsBehaviors.i18nBehavior
 */
class CellsNavigationBar extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
  static get is() {
    return 'cells-navigation-bar';
  }
  static get properties() {
    return {
      /**
       * Index of the current selected item.
       */
      selected: {
        type: Number,
        notify: true,
      },
      /**
       * Menu items.
       *
       * ```json
       * [{
       *   label: 'translation-key',
       *   icon: 'icon-id',
       *   id: 'itemId',
       *   link: '#/target'
       * }]
       * ```
       */
      items: {
        type: Array,
        observer: '_initNotificationList',
      },

      /**
       * Icon size of each item.
       */
      iconSize: {
        type: Number,
        value: 18,
      },

      preventNavigation: {
        type: Boolean,
        value: false,
      },

      /**
       * Boolean to disable the assignment
       * of selected item on click
       * to the property selected
       */
      disableSelected: Boolean,
    };
  }
  _initNotificationList() {
    var l = this.items.length;
    for (var i = 0; i < l; i++) {
      this.items[i].notificationIndex = 0;
    }
  }
  _onSelect(e) {
    if (this.preventNavigation) {
      e.preventDefault();
    }

    e.stopPropagation();

    if (!e.model.item.disabled) {
      if (!this.disableSelected) {
        this.selected = e.model.index;
      }

      this.dispatchEvent(new CustomEvent('selected-item', {
        bubbles: true,
        composed: true,
        detail: e.model.item,
      }));
    }
  }

  _checkedSelected(index, selected, disabled) {
    return index === selected && !disabled ? 'iron-selected' : '';
  }

  _computeLink(link, disabled) {
    if (!link || !link.trim() || disabled) {
      return '#';
    } else {
      return link;
    }
  }

  _computeSelectLink(disabled) {
    return disabled ? 'disabled' : '';
  }
  /**
   * Fired after selecting an item.
   * @event selected-item
   * @param {Object} event.model.item label, icon, id and link properties.
   */
}
customElements.define(CellsNavigationBar.is, CellsNavigationBar);
