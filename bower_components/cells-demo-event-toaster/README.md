![cells-demo-event-toaster screenshot](cells-demo-event-toaster.jpg)
# cells-demo-event-toaster

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://bbva-ether-cellscatalogs.appspot.com/?view=demo#/component/cells-demo-event-toaster)

`<cells-demo-event-toaster>` listens to a list of events and
opens a `paper-toast` element when those events are triggered.

In case it has a payload, it will also print it.

Example:

```html
<cells-demo-event-toaster events="[[eventsArray]]"></cells-demo-event-toaster>
```

## Styling

The class `full-width` can be used to expand the toast to the full viewport width.
