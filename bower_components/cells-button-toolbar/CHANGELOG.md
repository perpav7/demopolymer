
# CHANGELOG

## v3.0.0

**Breaking changes**

- Remove property `notifyEvent`
- CSS changes in component

- changed mixins names:

| Old mixin                          | New mixin                  |
|:-----------------------------------|:---------------------------|
| --cells-button-toolbar-container   | --cells-button-toolbar     |

**New features**

- Added new mixins:

| New mixins                                                  |
|:------------------------------------------------------------|
| --cells-button-toolbar-column                               |
| --cells-button-toolbar-column-ul-background                 |
| --cells-button-toolbar-column-ul                            |
| --cells-button-toolbar-column-li                            |
| --cells-button-toolbar-column-button-icon                   |
| --cells-button-toolbar-column-button-image                  |
| --cells-button-toolbar-column-button-text                   |
| --cells-button-toolbar-column-button-focus                  |
| --cells-button-toolbar-column-button-focus-text             |
| --cells-button-toolbar-column-button-not-active             |
| --cells-button-toolbar-column-button-not-active-icon        |
| --cells-button-toolbar-column-button-not-active-text-color  |
| --cells-button-toolbar-column-button-not-active-text        |
| --cells-button-toolbar-column-button-active-color           |
| --cells-button-toolbar-column-button-active                 |
| --cells-button-toolbar-column-button-active-icon            |
| --cells-button-toolbar-column-button-active-text-color      |
| --cells-button-toolbar-column-button-active-text            |
| --cells-button-toolbar-ul-height                            |
| --cells-button-toolbar-button-background                    |
| --cells-button-toolbar-button-focus-text-color              |
| --cells-button-toolbar-button-focus-text                    |
| --cells-button-toolbar-active-icon-color                    |
| --cells-button-toolbar-text-color                           |
