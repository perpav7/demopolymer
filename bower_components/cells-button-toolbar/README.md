# cells-button-toolbar

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/ccells-button-toolbar)

`<cells-button-toolbar>` shows a menu with a list of options (text and icon).

Basic example:

```html
<cells-button-toolbar tools="someTools"></cells-button-toolbar>
```

Multi selection example:

```html
<cells-button-toolbar tools="someTools" is-multi></cells-button-toolbar>
```

Custom icon size example:

```html
<cells-button-toolbar tools="someTools" icon-size="icon-size-24"></cells-button-toolbar>
```

## Data model

### tools

+ **text**: text to show
+ **hiddenText**: set to true to hide the text in an a11y-friendly way
+ **iconCode**: icon code for cells-atom-icon
+ **imageUrl**: image url to show an image instead of an icon
+ **imageAlt**: alt text for the image
+ **id**: id
+ **isActive**: when some tool is active this property is `true`

Text attribute is mandatory for accessibility. Also, imageAlt must be provided if imageUrl is included.

```json
  [
    {
      text: 'puntos',
      iconCode: 'coronita:rewards',
      id: 'puntos',
      isActive: false
    },
    {
      text: 'descuentos',
      iconCode: 'coronita:transfer',
      id: 'descuentos',
      isActive: true
    },
    {
      text: 'sin intereses',
      iconCode: 'coronita:calendar',
      id: 'intereses',
      isActive: false
    },
    {
      text: 'filtros',
      iconCode: 'coronita:filter',
      id: 'filtros',
      isActive: false
    },
    {
      text: 'Línea Bancomer',
      imageUrl: 'http://bbva-files.s3.amazonaws.com/cells/assets/shoppingapp/logo-linea-bancomer.png',
      id: 'lineaBancomer',
      isActive: false
    }
  ]
```

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.


## Styling

The following custom properties and mixins are available for styling:

| Custom property                                             | Description                                        | Default                 |
|:------------------------------------------------------------|:---------------------------------------------------|:-----------------------:|
| --cells-button-toolbar                                      | empty mixin host                                   | {}                      |
| --cells-button-toolbar-ul                                   | mixin for the list of tools                        | {}                      |
| --cells-button-toolbar-ul-background                        | background for ul                                  | #072146                 |
| --cells-button-toolbar-ul-height                            | height for ul                                      | rem(54px)               |
| --cells-button-toolbar-li                                   | mixin for the tool                                 | {}                      |
| --cells-button-toolbar-button                               | mixin for the button                               | {}                      |
| --cells-button-toolbar-button-background                    | background for button                              | transparent             |
| --cells-button-toolbar-button-color                         | color for button text                              | rgba(255,255,255,0.5)   |
| --cells-button-toolbar-button-focus                         | mixin for the button focus                         | {}                      |
| --cells-button-toolbar-button-color-focus                   | var for button color focus                         | #fff                    |
| --cells-button-toolbar-text                                 | mixin for the button text                          | {}                      |
| --cells-button-toolbar-text-color                           | color of text                                      | rgba(255,255,255,0.5)   |
| --cells-button-toolbar-button-focus-text-color              | color for host column button focus text            | #fff                    |
| --cells-button-toolbar-button-focus-text                    | empty mixin for host column button focus text      | {}                      |
| --cells-button-toolbar-icon                                 | empty mixin for the button icon                    | {}                      |
| --cells-button-toolbar-image                                | empty mixin for the button image                   | {}                      |
| --cells-button-toolbar-not-active                           | empty mixin for the inactive button                | {}                      |
| --cells-button-toolbar-not-active-icon                      | empty mixin for the inactive button icon           | {}                      |
| --cells-button-toolbar-not-active-text                      | empty mixin for the inactive button text           | {}                      |
| --cells-button-toolbar-active                               | empty mixin for the active button                  | {}                      |
| --cells-button-toolbar-active-icon                          | empty mixin for the active button icon             | {}                      |
| --cells-button-toolbar-active-icon-color                    | color for the active button icon                   | #fff                    |
| --cells-button-toolbar-button-color-active                  | color for the active button text                   | #fff                    |
| --cells-button-toolbar-active-text                          | empty mixin for the active button text             | {}                      |
| --cells-button-toolbar-column                               | empty mixin for host column                        | {}                      |
| --cells-button-toolbar-column-ul                            | empty mixin for host column container ul           | {}                      |
| --cells-button-toolbar-column-ul-background                 | background for host column background              | #072146                 |
| --cells-button-toolbar-column-li                            | empty mixin for host column li                     | {}                      |
| --cells-button-toolbar-column-button-icon                   | empty mixin for host column container icon         | {}                      |
| --cells-button-toolbar-column-button-image                  | empty mixin for host column container image        | {}                      |
| --cells-button-toolbar-column-button-text                   | empty mixin for host column button text            | {}                      |
| --cells-button-toolbar-column-button-focus                  | empty mixin for host column button focus           | {}                      |
| --cells-button-toolbar-column-button-focus-text             | empty mixin for host column button focus text      | {}                      |
| --cells-button-toolbar-column-button-not-active             | empty mixin for host column button not active      | {}                      |
| --cells-button-toolbar-column-button-not-active-icon        | empty mixin for host column button not active icon | {}                      |
| --cells-button-toolbar-column-button-not-active-text-color  | color for host column button not active text color | rgba(255,255,255,0.5)   |
| --cells-button-toolbar-column-button-not-active-text        | empty mixin for host column button not active text | {}                      |
| --cells-button-toolbar-column-button-active-color           | color for host column button active color          | #fff                    |
| --cells-button-toolbar-column-button-active                 | empty mixin for host column button active          | {}                      |
| --cells-button-toolbar-column-button-active-icon            | empty mixin for host column button active icon     | {}                      |
| --cells-button-toolbar-column-button-active-text-color      | color for host column button active text color     | #fff                    |
| --cells-button-toolbar-column-button-active-text            | empty mixin for host column button active text     | {}                      |


## Api Reference

### Properties

+ isMulti {Boolean} Default: false: Determines if it's possible to select more than one option
+ selected {Array} Default: []: List of selected options
+ tools {Array} Default: []: Options for toolbar
+ iconSize {String} Default: 'icon-size-22': Determines the size of the icon
