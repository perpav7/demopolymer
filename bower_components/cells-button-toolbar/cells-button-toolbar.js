(function() {

  'use strict';

  Polymer({

    is: 'cells-button-toolbar',
    properties: {
      /**
       * Options for toolbar
       * @type {Array}
       */
      tools: {
        type: Array,
        observer: '_refreshSelected'
      },
      /**
       * Determines if it's possible to select more than one option
       * @type {Boolean}
       */
      isMulti: {
        type: Boolean,
        value: false
      },
      /**
       * Size of the icons. Use a class icon-size-<size> to set
       * the width and height of the icon. Available
       * sizes go from 8 to 64px (only even numbers).
       * @type {String}
       */
      iconSize: {
        type: String,
        value: 'icon-size-22'
      },
      /**
       * List of selected options
       * @type {Array}
       */
      selected: {
        type: Array,
        value: function() {
          return [];
        }
      },
      /**
       * Aria role of toolbar
       * @type {String}
       */
      mainRole: {
        type: String,
        value: 'menu'
      }
    },

    /**
     * Returns css class if item is active
     * @param  {Boolean} isActive Determines whether the button is active
     * @return {String}           Name of the active/inactive class
     */
    _getActiveClass: function(isActive) {
      return isActive ? 'active' : '';
    },
    /**
     * Returns true or false for aria checked if item is active
     * @param  {Boolean} isActive Determines whether the button is active
     * @return {Boolean}
     */
    _getAriaChecked: function(isActive, e) {
      return isActive ? 'true' : 'false';
    },
    /**
     * Returns item role based on isMulti property
     * @param  {Boolean} isMulti Determines whether the toolbar is single or multi selection
     * @return {String}  Name of the role to be applied (menuitemcheckbox/menuitemradio)
     */
    _getItemRole: function(isMulti) {
      return isMulti ? 'menuitemcheckbox' : 'menuitemradio';
    },
    /**
     * Changes tools status
     * @param {Event} e Button click event
     */
    _setActive: function(e) {
      var model = e.model;
      var ev = Polymer.dom(e);
      var target = ev.rootTarget;
      if (this.isMulti) {
        model.set('item.isActive', !model.item.isActive);
        target.setAttribute('aria-checked', model.item.isActive);
      } else {
        this.tools.forEach(function(tool, index) {
          this.set(['tools', index, 'isActive'], false);
        }, this);

        model.set('item.isActive', true);
        target.setAttribute('aria-checked', model.item.isActive);
        /**
         * Fired when an item is clicked with the ID as payload
         * @event cells-button-toolbar-clicked
         * @param  {{String}} id [the id of the item]
         */
        this.dispatchEvent(new CustomEvent('cells-button-toolbar-clicked', {
          bubbles: true,
          composed: true,
          detail: model.item.id
        }));
      }

      this._refreshSelected(this.tools);
    },
    /**
     * Updates `selected` property with active options
     * @param  {Array} tools Options for toolbar
     */
    _refreshSelected: function(tools) {
      var selected = tools.filter(function(tool) {
        return tool.isActive;
      });

      this.set('selected', selected);
    }

  });
}());
