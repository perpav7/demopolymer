/**
 * @customElement
 * @summary Shows a control and a collapsible layer associated to it
 * @polymer
 * @demo demo/index.html
 * @extends {Polymer.Element}
 */
class cellsDropdownLayer extends Polymer.Element {
  static get is() {
    return 'cells-dropdown-layer';
  }

  static get properties() {
    return {
      /**
       * Set dropdown as disabled
       */
      disabled: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      /**
       * Icon to show to the left of the label
       */
      icon: {
        type: String,
        value: ''
      },

      /**
       * Prefix for the label
       */
      prefix: {
        type: String,
        observer: 'checkControlMinWidth'
      },

      /**
       * Text label for control
       **/
      label: {
        type: String,
        value: '',
        observer: 'checkControlMinWidth'
      },

      /**
       * Disabled icon besides underline
       */
      iconDisabled: {
        type: String,
        value: 'coronita:block'
      },

      /**
       * Icon to show besides underline
       */
      iconOpen: {
        type: String,
        value: 'coronita:unfold'
      },

      /**
       * If true, layer is opened
       */
      opened: {
        type: Boolean,
        value: false,
        notify: true,
        reflectToAttribute: true,
        observer: '_opened'
      },

      /**
       * Animating status of layer
       */
      animating: {
        type: Boolean,
        readOnly: true,
        reflectToAttribute: true
      },

      /**
       * Error message
       */
      error: {
        type: String
      },

      /**
       * If true, component has error state
       */
      hasError: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        computed: '_setErrorState(error)'
      },

      /**
       * Max width of layer, autocalculated from viewport size
       */
      _maxWidth: {
        type: Number
      },

      /**
       * If true, layer can be less wide than control
       */
      controlAsMinWidthOff: {
        type: Boolean,
        observer: 'checkControlMinWidth'
      },

      /**
       * Base min width, calculated from control Width, or 80, based on controlAsMinWidthOff
       */
      _minWidth: {
        type: Number
      },

      /**
       * Max height of layer, autocalculated from viewport size
       */
      _maxHeight: {
        type: Number
      },

      /**
       * Min height of layer
       */
      minHeight: {
        type: Number,
        value: 64
      },

      /**
       * Horizontal direction in which layer will open. Allowed values are 'left-to-right', 'right-to-left', 'centered' and 'auto'
       */
      openingX: {
        type: String,
        value: 'auto'
      },

      /**
       * Vertical direction in which layer will open. Allowed values are 'top-to-bottom', 'bottom-to-top' and 'auto'
       */
      openingY: {
        type: String,
        value: 'auto'
      },

      /**
       * Minimum distance from layer limits to viewport
       */
      safeMargin: {
        type: Number,
        value: 32
      },

      /**
       * If true, control has focus
       */
      focused: {
        type: Boolean,
        readOnly: true,
        reflectToAttribute: true
      },

      /**
       * If true, layer can overflow screen width limits
       */
      overflowScreenWidth: {
        type: Boolean,
        value: false
      },

      /**
       * If true, layer can overflow screen height limits
       */
      overflowScreenHeight: {
        type: Boolean,
        value: false
      }
    };
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('keydown', function(e) {
      this._keyHandler(e);
    }.bind(this));
  }

  /**
   * Set base min width of layer: 80, or control width
   */
  checkControlMinWidth() {
    requestAnimationFrame(() => {
      this._minWidth = this.controlAsMinWidthOff ? 80 : this.offsetWidth;
    });
  }

  /**
   * Toggles opened state on click
   */
  _clickHandler() {
    if (!this.disabled) {
      document.removeEventListener('click', this._docHandler);
      document.removeEventListener('focusin', this._docHandler);
      this._setAnimating(true);
      setTimeout(function() {
        this.opened = !this.opened;
      }.bind(this), 16);
    }
  }

  /**
   * Set focus on button
   */
  focus() {
    this.shadowRoot.querySelector('.control-text').focus();
  }

  /**
   * Closes opened layer if Esc key is pressed
   */
  _keyHandler(e) {
    if (e.keyCode === 27 && this.opened) {
      e.preventDefault();
      this.close();
    }
  }

  /**
   * Set hasError property based on error property
   */
  _setErrorState(error) {
    return !!error;
  }

  /**
   * Closes layer and removes listeners
   */
  close() {
    document.removeEventListener('click', this._docHandler);
    document.removeEventListener('focusin', this._docHandler);
    if (this.opened) {
      this._setAnimating(true);
      setTimeout(function() {
        this.opened = false;
      }.bind(this), 16);
    }
  }

  /**
   * Opens layer
   */
  open() {
    if (!this.opened) {
      this._setAnimating(true);
      setTimeout(function() {
        this.opened = true;
      }.bind(this), 16);
    }
  }

  /**
   * Set layer sizes, and adds listeners to close layer on outside focus/click
   */
  _opened(newValue) {
    this._docHandler = function(ev) {
      if (ev.composedPath().indexOf(this) < 0 || ev.composedPath()[0] === this.shadowRoot.querySelector('.layer')) {
        this.close();
      }
    }.bind(this);
    if (this.opened) {
      this._setAnimating(true);
      this._checkMax();
      document.addEventListener('click', this._docHandler);
      document.addEventListener('focusin', this._docHandler);
    } else {
      document.removeEventListener('click', this._docHandler);
      document.removeEventListener('focusin', this._docHandler);
    }
  }

  /**
   * Sets direction classes on layer
   */
  _checkMax() {
    var elemRect = this.getBoundingClientRect();
    var dom = document.documentElement;
    /* istanbul ignore next */
    var viewW = Math.max(dom.clientWidth, window.innerWidth || 0);
    /* istanbul ignore next */
    var viewH = Math.max(dom.clientHeight, window.innerHeight || 0);
    var horizontal = this._checkHorizontal(elemRect, viewW);
    var vertical = this._checkVertical(elemRect, viewH);
    this._layerDirection = vertical + ' ' + horizontal;
  }

  /**
   * Set max width of layer and returns horizontal direction
   */
  _checkHorizontal(opener, view) {
    var rightSpace = view - opener.left - this.safeMargin;
    var leftSpace = opener.right - this.safeMargin;
    var max;
    var result;
    switch (this.openingX) {
      case 'left-to-right':
        max = rightSpace;
        result = 'left';
        break;
      case 'right-to-left':
        max = leftSpace;
        result = 'right';
        break;
      case 'centered':
        max = Math.min((opener.right - this.safeMargin - (opener.right - opener.left) / 2), (view - this.safeMargin - opener.left - ((opener.right - opener.left) / 2))) * 2;
        result = 'centered';
        break;
      default:
        if (rightSpace < this._minWidth && leftSpace > rightSpace) {
          max = leftSpace;
          result = 'right';
        } else {
          max = rightSpace;
          result = 'left';
        }
        break;
    }
    this._maxWidth = this.overflowScreenWidth ? null : max;
    return result;
  }

  _checkMaxWidth(_maxWidth) {
    return _maxWidth ? `max-width:${_maxWidth}px;` : '';
  }

  /**
   * Set max height of layer and returns vertical direction
   */
  _checkVertical(opener, view) {
    var bottomSpace = view - opener.bottom - this.safeMargin;
    var topSpace = opener.top - this.safeMargin;
    var max;
    var result;
    switch (this.openingY) {
      case 'top-to-bottom':
        max = bottomSpace;
        result = 'top';
        break;
      case 'bottom-to-top':
        max = topSpace;
        result = 'bottom';
        break;
      default:
        if (bottomSpace < this.minHeight && topSpace > bottomSpace) {
          max = topSpace;
          result = 'bottom';
        } else {
          max = bottomSpace;
          result = 'top';
        }
        break;
    }
    this._maxHeight = this.overflowScreenHeight ? null : max;
    return result;
  }

  _checkMaxHeight(_maxHeight) {
    return _maxHeight ? `max-height:${_maxHeight}px;` : '';
  }

  /**
   * Reset layer sizes
   */
  _fixSize(ev) {
    this._setAnimating(false);
    var layer = this.shadowRoot.querySelector('.layer');
    var layerContent = this.shadowRoot.querySelector('.layer-content');
    layer.style.width = this.opened ? layerContent.offsetWidth + 1 + 'px' : '100vw';
    if (!this.opened) {
      this._layerDirection = '';
      this._maxWidth = 0;
      this._maxHeight = 0;
    }
  }

  _focus() {
    this._setFocused(true);
  }

  _blur() {
    this._setFocused(false);
  }
}

customElements.define(cellsDropdownLayer.is, cellsDropdownLayer);
