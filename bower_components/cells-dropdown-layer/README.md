# cells-dropdown-layer

![cells-dropdown-layer screenshot](cells-dropdown-layer.png)

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://bbva-ether-cellscatalogs.appspot.com/?view=demo#/component/cells-dropdown-layer)

Component to show a control and an expansible content. Activating the control will show a layer which will be filled with the component contents.

```html
<cells-dropdown-layer label="Lorem ipsum">
  Content...
</cells-dropdown-layer>
```

You can set the label for the control, as well as an optional icon for it. You can use a prefix text for the control, too. You can also set 'disabled' state. The layer can be closed with the Esc key, activating the control again, or clicking/focusing anything outside it.

```html
<cells-dropdown-layer label="Lorem ipsum" prefix="Lorem ipsum: " disabled>
  Content...
</cells-dropdown-layer>
<cells-dropdown-layer label="Lorem ipsum" icon="coronita:creditcard">
  Content...
</cells-dropdown-layer>
```

The layer tends to open from top left to bottom right by default, but it will adjust its opening direction based on its content size and the available space in the viewport. You can use the `safeMargin` property to specify the "safe distance" (in pixels) to keep from the layer to the viewport limits. Opening direction can be specified using the `openingX` and `openingY` attributes. `opening-x` can receive 'left-to-right', 'right-to-left' or 'centered' as values, while `opening-y` can receive 'top-to-bottom' or 'bottom-to-top'.

```html
<cells-dropdown-layer label="Lorem ipsum" opening-x="centered" opening-y="bottom-to-top" safe-margin="24">
  Content...
</cells-dropdown-layer>
```

Layer width and height will adapt to its content, although it won't be smaller than the control width. You can allow the layer to be smaller than the control width setting the `controlAsMinWidthOff` property to true. Also, you can use CSS variables to set min-width, min-height, max-width and max-height for the layer.

```html
<cells-dropdown-layer label="Lorem ipsum" control-as-min-width-off>
  Content...
</cells-dropdown-layer>
```

The component will show an error status when setting its `error` property with an object containing a 'msg' (for example, `{ "msg": "You must select an option"}`).

The following helper classes are provided in the component styles:
- 'small': makes the component control smaller.
- 'dark': changes the component colors to improve contrast for dark backgrounds.
- 'icon-only': leaves just the icon as visible element in the control.

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

| Custom property                                               | Description                                                               | Default |
|:--------------------------------------------------------------|:--------------------------------------------------------------------------|:-------:|
| --cells-dropdown-layer-value-color                            | Value color                                                               | #565C67 |
| --cells-dropdown-layer-underline-color                        | Underline color                                                           | #C6C8CF |
| --cells-dropdown-layer-icon-color                             | Icon color                                                                | #565C67 |
| --cells-dropdown-layer-animation-time                         | Transitions duration                                                      |  0.3s   |
| --cells-dropdown-layer                                        | Empty mixin for component                                                 |   {}    |
| --cells-dropdown-layer-dark-value-color                       | Value color with dark class                                               | #D0D1D4 |
| --cells-dropdown-layer-dark-underline-color,                  | Underline color with dark class                                           | #565C67 |
| --cells-dropdown-layer-dark-icon-color                        | Icon color with dark class                                                | #A3A8AD |
| --cells-dropdown-layer-dark                                   | Empty mixin for component with dark class                                 |   {}    |
| --cells-dropdown-layer-opener                                 | Empty mixin for opener block                                              |   {}    |
| --cells-dropdown-layer-control-text                           | Empty mixin for control text                                              |   {}    |
| --cells-dropdown-layer-control-prefix                         | Empty mixin for control prefix                                            |   {}    |
| --cells-dropdown-layer-control-icon-icon                      | Empty mixin for control icon iron-icon                                    |   {}    |
| --cells-dropdown-layer-control-icon                           | Empty mixin for control icon block                                        |   {}    |
| --cells-dropdown-layer-control                                | Empty mixin for control                                                   |   {}    |
| --cells-dropdown-layer-line-underline-after                   | Empty mixin for underline after                                           |   {}    |
| --cells-dropdown-layer-line-underline                         | Empty mixin for underline block                                           |   {}    |
| --cells-dropdown-layer-line-icon-open                         | Empty mixin for open icon                                                 |   {}    |
| --cells-dropdown-layer-line-icon-disabled                     | Empty mixin for disabled icon                                             |   {}    |
| --cells-dropdown-layer-line-icon                              | Empty mixin for icon                                                      |   {}    |
| --cells-dropdown-layer-line-icons                             | Empty mixin for icons block                                               |   {}    |
| --cells-dropdown-layer-line                                   | Empty mixin for line block                                                |   {}    |
| --cells-dropdown-layer-error-text                             | Empty mixin for error text                                                |   {}    |
| --cells-dropdown-layer-layer-color                            | Layer color                                                               | #D0D1D4 |
| --cells-dropdown-layer-layer-animating                        | Empty mixin for layer when animating                                      |   {}    |
| --cells-dropdown-layer-layer-max-width                        | Layer max width                                                           |  none   |
| --cells-dropdown-layer-layer-min-width                        | Layer min width                                                           |    0    |
| --cells-dropdown-layer-layer-max-height                       | Layer max height                                                          |  none   |
| --cells-dropdown-layer-layer-min-height                       | Layer min height                                                          |    0    |
| --cells-dropdown-layer-wrapper                                | Empty mixin for layer wrapper                                             |   {}    |
| --cells-dropdown-layer-content-bg-color                       | Layer content background color                                            | #343B49 |
| --cells-dropdown-layer-content                                | Empty mixin for layer content                                             |   {}    |
| --cells-dropdown-layer-top                                    | Empty mixin for layer on top opening                                      |   {}    |
| --cells-dropdown-layer-bottom                                 | Empty mixin for layer on bottom opening                                   |   {}    |
| --cells-dropdown-layer-left-top                               | Empty mixin for layer on left top opening                                 |   {}    |
| --cells-dropdown-layer-left-bottom                            | Empty mixin for layer on left bottom opening                              |   {}    |
| --cells-dropdown-layer-left                                   | Empty mixin for layer on left opening                                     |   {}    |
| --cells-dropdown-layer-right-top                              | Empty mixin for layer on right top opening                                |   {}    |
| --cells-dropdown-layer-right-bottom                           | Empty mixin for layer on right bottom opening                             |   {}    |
| --cells-dropdown-layer-right                                  | Empty mixin for layer on right opening                                    |   {}    |
| --cells-dropdown-layer-centered-wrapper                       | Empty mixin for layer wrapper on centered opening                         |   {}    |
| --cells-dropdown-layer-centered-top                           | Empty mixin for layer on centered top opening                             |   {}    |
| --cells-dropdown-layer-centered-bottom                        | Empty mixin for layer on centered bottom opening                          |   {}    |
| --cells-dropdown-layer-centered                               | Empty mixin for layer on centered opening                                 |   {}    |
| --cells-dropdown-layer-layer                                  | Empty mixin for layer                                                     |   {}    |
| --cells-dropdown-layer-hovered-value-color                    | Value color when hovered/focused                                          | #224FBD |
| --cells-dropdown-layer-hovered-underline-color                | Underline color when hovered/focused                                      | #224FBD |
| --cells-dropdown-layer-hovered-icon-color                     | Icon color when hovered/focused                                           | #224FBD |
| --cells-dropdown-layer-hovered-line-underline-after           | Empty mixin for underline after when hovered/focused                      |   {}    |
| --cells-dropdown-layer-hovered                                | Empty mixin for hovered component                                         |   {}    |
| --cells-dropdown-layer-dark-hovered-value-color               | Value color when hovered/focused with dark class                          |  #FFF   |
| --cells-dropdown-layer-dark-hovered-underline-color           | Underline color when hovered/focused with dark class                      |  #FFF   |
| --cells-dropdown-layer-dark-hovered-icon-color                | Icon color when hovered/focused with dark class                           |  #FFF   |
| --cells-dropdown-layer-dark-hovered                           | Empty mixin for hovered component with dark class                         |   {}    |
| --cells-dropdown-layer-opened-value-color                     | Value color when opened                                                   | #224FBD |
| --cells-dropdown-layer-opened-underline-color                 | Underline color when opened                                               | #224FBD |
| --cells-dropdown-layer-opened-icon-color                      | Icon color when opened                                                    | #224FBD |
| --cells-dropdown-layer-opened-line-underline-after            | Empty mixin for underline after when opened                               |   {}    |
| --cells-dropdown-layer-opened-line-icon-open                  | Empty mixin for open icon when opened                                     |   {}    |
| --cells-dropdown-layer-opened-line                            | Empty mixin for line when opened                                          |   {}    |
| --cells-dropdown-layer-opened-layer                           | Empty mixin for layer when opened                                         |   {}    |
| --cells-dropdown-layer-opened                                 | Empty mixin for component when opened                                     |   {}    |
| --cells-dropdown-layer-dark-opened-value-color                | Value color when opened with dark class                                   |  #FFF   |
| --cells-dropdown-layer-dark-opened-underline-color            | Underline color when opened with dark class                               |  #FFF   |
| --cells-dropdown-layer-dark-opened-icon-color                 | Icon color when opened with dark class                                    |  #FFF   |
| --cells-dropdown-layer-dark-opened                            | Empty mixin for component when opened with dark class                     |   {}    |
| --cells-dropdown-layer-disabled-value-color                   | Value color when disabled                                                 | #A3A8AD |
| --cells-dropdown-layer-disabled-underline-color               | Underline color when disabled                                             | #D0D1D4 |
| --cells-dropdown-layer-disabled-icon-color                    | Icon color when disabled                                                  | #A3A8AD |
| --cells-dropdown-layer-disabled-line-icon-disabled            | Empty mixin for disabled icon when disabled                               |   {}    |
| --cells-dropdown-layer-disabled-line-icon-open                | Empty mixin for open icon when disabled                                   |   {}    |
| --cells-dropdown-layer-disabled-line-icons                    | Empty mixin for icons when disabled                                       |   {}    |
| --cells-dropdown-layer-disabled                               | Empty mixin for disabled component                                        |   {}    |
| --cells-dropdown-layer-dark-disabled-value-color              | Value color when disabled with dark class                                 | #A3A8AD |
| --cells-dropdown-layer-dark-disabled-underline-color          | Underline color when disabled with dark class                             | #565C67 |
| --cells-dropdown-layer-dark-disabled-icon-color               | Icon color when disabled with dark class                                  | #A3A8AD |
| --cells-dropdown-layer-dark-disabled                          | Empty mixin for disabled component with dark class                        |   {}    |
| --cells-dropdown-layer-with-error-value-color                 | Value color on error                                                      | #F970B5 |
| --cells-dropdown-layer-with-error-underline-color             | Underline color on error                                                  | #F970B5 |
| --cells-dropdown-layer-with-error-icon-color                  | Icon color on error                                                       | #F970B5 |
| --cells-dropdown-layer-with-error-text                        | Empty mixin for error text on error                                       |   {}    |
| --cells-dropdown-layer-with-error                             | Empty mixin for component on error                                        |   {}    |
| --cells-dropdown-layer-dark-with-error-value-color            | Value color on error with dark class                                      | #F970B5 |
| --cells-dropdown-layer-dark-with-error-underline-color        | Underline color on error with dark class                                  | #F970B5 |
| --cells-dropdown-layer-dark-with-error-icon-color             | Icon color on error with dark class                                       | #F970B5 |
| --cells-dropdown-layer-dark-with-error                        | Empty mixin for component on error with dark class                        |   {}    |
| --cells-dropdown-layer-small-control-text                     | Empty mixin for control text with small class                             |   {}    |
| --cells-dropdown-layer-small-control-icon                     | Empty mixin for control icon with small class                             |   {}    |
| --cells-dropdown-layer-small-layer-top                        | Empty mixin for layer top with small class                                |   {}    |
| --cells-dropdown-layer-small-layer-content                    | Empty mixin for layer content with small class                            |   {}    |
| --cells-dropdown-layer-small-error-text                       | Empty mixin for error text with small class                               |   {}    |
| --cells-dropdown-layer-small                                  | Empty mixin for component with small class                                |   {}    |
| --cells-dropdown-layer-small-opened-line-icon-open            | Empty mixin for icon open when opened with small class                    |   {}    |
| --cells-dropdown-layer-small-opened                           | Empty mixin when opened with small class                                  |   {}    |
| --cells-dropdown-layer-icon-only-control-icon                 | Empty mixin for control icon with icon only class                         |   {}    |
| --cells-dropdown-layer-icon-only-control-text                 | Empty mixin for control text with icon only class                         |   {}    |
| --cells-dropdown-layer-icon-only-control                      | Empty mixin for control with icon only class                              |   {}    |
| --cells-dropdown-layer-icon-only-line-underline-after         | Empty mixin for underline after with icon only class                      |   {}    |
| --cells-dropdown-layer-icon-only-line-icons                   | Empty mixin for icons block with icon only class                          |   {}    |
| --cells-dropdown-layer-icon-only-line                         | Empty mixin for line block with icon only class                           |   {}    |
| --cells-dropdown-layer-icon-only                              | Empty mixin for component with icon only class                            |   {}    |
| --cells-dropdown-layer-icon-only-hovered-line-underline-after | Empty mixin for underline after when hovered/focused with icon only class |   {}    |
| --cells-dropdown-layer-icon-only-hovered                      | Empty mixin for component when hovered/focused with icon only class       |   {}    |
