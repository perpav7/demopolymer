# cells-dropdown-menu

![cells-dropdown-menu screenshot](cells-dropdown-menu.png)

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://bbva-ether-cellscatalogs.appspot.com/?view=demo#/component/cells-dropdown-menu)

This component uses `cells-dropdown-layer` to show a list of selectable items which can be checked. It works as a select component, and expects an array of items to build the list.

```html
<cells-dropdown-menu placeholder="Default text" items='[...]'></cells-dropdown-layer>
```

```javascript
[{
  'name': 'Lorem ipsum',
  'value': 'loremipsum'
}, {
  'name': 'Lorem ipsum 2',
  'value': 'loremipsum2',
  'icon': 'coronita:creditcard'
}]
```

The menu can be set as required (which will show an error if no option is checked) or disabled. You can limit the amount of visible options at once with the 'maxItemsShown' property.

You can use the 'selected' property to set the currently selected item by index, as well as the 'value' property for the value of the current index.

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

| Custom property                                           | Description                                     | Default |
|:----------------------------------------------------------|:------------------------------------------------|:-------:|
| --cells-dropdown-menu-value-color                         | Text color                                      | #D0D1D4 |
| --cells-dropdown-menu-underline-color                     | Underline color                                 | #565C67 |
| --cells-dropdown-menu-highlighted-color                   | Highlighted color                               |  #FFF   |
| --cells-dropdown-menu                                     | Empty mixin for component                       |   {}    |
| --cells-dropdown-menu-haserror-layer-icon-color           | Layer icon color with error                     | #F970B5 |
| --cells-dropdown-menu-haserror-layer-underline-color      | Layer underline color with error                | #F970B5 |
| --cells-dropdown-menu-haserror-dark-layer-icon-color      | Layer icon color with dark class and error      | #F970B5 |
| --cells-dropdown-menu-haserror-dark-layer-underline-color | Layer underline color with dark class and error | #F970B5 |
| --cells-dropdown-menu-haserror                            | Empty mixin for component with error            |   {}    |
| --cells-dropdown-menu-layer                               | Empty mixin for layer                           |   {}    |
| --cells-dropdown-menu-lists                               | Empty mixin for list                            |   {}    |
| --cells-dropdown-menu-item-error-value-color              | Text color for item with error                  | #F970B5 |
| --cells-dropdown-menu-item-error-highlighted-color        | Highlighted color for item with error           | #F970B5 |
| --cells-dropdown-menu-item-error-underline-color          | Underline color for item with error             | #F970B5 |
| --cells-dropdown-menu-item-error                          | Empty mixin for item with error                 |   {}    |
| --cells-dropdown-menu-item-last                           | Empty mixin for last item                       |   {}    |
| --cells-dropdown-menu-item                                | Empty mixin for item                            |   {}    |
| --cells-dropdown-menu-control-icon-selected               | Empty mixin for selected item icon              |   {}    |
| --cells-dropdown-menu-control-icon                        | Empty mixin for item icon                       |   {}    |
| --cells-dropdown-menu-control-label-selected              | Empty mixin for selected label                  |   {}    |
| --cells-dropdown-menu-control-label                       | Empty mixin for label                           |   {}    |
| --cells-dropdown-menu-control-icon-check-checked          | Empty mixin for check icon when checked         |   {}    |
| --cells-dropdown-menu-control-icon-check                  | Empty mixin for check icon                      |   {}    |
| --cells-dropdown-menu-control                             | Empty mixin for item control block              |   {}    |
| --cells-dropdown-menu-underline-selected                  | Empty mixin for selected underline              |   {}    |
| --cells-dropdown-menu-underline                           | Empty mixin for underline                       |   {}    |
| --cells-dropdown-menu-line                                | Empty mixin for line block                      |   {}    |
