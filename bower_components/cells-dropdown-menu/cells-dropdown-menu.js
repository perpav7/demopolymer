/**
 * @customElement
 * @summary Show a custom select component using cells-dropdown-layer
 * @polymer
 * @demo demo/index.html
 * @extends {Polymer.Element}
 */
class cellsDropdownMenu extends Polymer.Element {
  static get is() {
    return 'cells-dropdown-menu';
  }

  static get properties() {
    return {
      /**
       * Items to show in the layer
       * [{
       *   name: 'Item label',
       *   value: 'some-value',
       *   icon: 'iconset:iconname'
       * }]
       */
      items: {
        type: Array,
        value: function() {
          return [];
        },
        observer: '_itemsObserver',
      },

      /**
       * Check icon
       */
      iconCheck: {
        type: String,
        value: 'coronita:checkmark',
      },

      /**
       * Disabled state of the component
       */
      disabled: {
        type: Boolean,
        reflectToAttribute: true,
      },

      /**
       * Max items to show at once in the layer (sets a max Height in the layer)
       */
      maxItemsShown: {
        type: Number,
        observer: '_maxItemsShown',
      },

      /**
       * Text to show when no option is selected
       */
      placeholder: {
        type: String,
        value: '',
      },

      /**
       * Current label text
       */
      _label: {
        type: String,
        value: '',
      },

      /**
       * Prefix for label
       */
      prefix: {
        type: String,
      },

      /**
       * Required status of the component
       */
      required: {
        type: Boolean,
        value: false,
      },

      /**
       * Message to show when no option is selected in a required dropdown
       */
      requiredMsg: {
        type: String,
        value: 'You must select an option',
      },

      /**
       * Opened status
       */
      opened: {
        type: Boolean,
        reflectToAttribute: true,
        observer: '_opened',
      },

      /**
       * Currently selected item
       */
      selected: {
        type: Number,
        value: -1,
        observer: '_selectedObserver',
        notify: true,
      },

      /**
       * Value of currently selected item
       */
      value: {
        type: String,
        notify: true,
        observer: '_valueChange',
      },

      /**
       * Icon to show to the left of the layer control label
       */
      controlIcon: {
        type: String,
      },

      /**
       * Disabled icon besides underline
       */
      iconDisabled: {
        type: String,
      },

      /**
       * Icon to show besides underline
       */
      iconOpen: {
        type: String,
      },

      /**
       * If true, layer can be less wide than control
       */
      controlAsMinWidthOff: {
        type: Boolean,
      },

      /**
       * Min height of layer
       */
      minHeight: {
        type: Number,
      },

      /**
       * Horizontal direction in which layer will open. Allowed values are 'left-to-right', 'right-to-left', 'centered' and 'auto'
       */
      openingX: {
        type: String,
      },

      /**
       * Vertical direction in which layer will open. Allowed values are 'top-to-bottom', 'bottom-to-top' and 'auto'
       */
      openingY: {
        type: String,
      },

      /**
       * Minimum distance from layer limits to viewport
       */
      safeMargin: {
        type: Number,
      },

      /**
       * Error message
       */
      error: {
        type: String,
      },

      /**
       * Boolean used to indentify if some item has an error property
       */
      hasError: {
        type: Boolean,
        reflectToAttribute: true,
      },
    };
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('keydown', function(e) {
      this._keyHandler(e);
    }.bind(this));
    this._layerWidth();
  }

  /**
   * Open layer
   */
  open() {
    this.$.layer.open();
  }

  /**
   * Close layer
   */
  close() {
    this.$.layer.close();
  }

  _layerWidth() {
    this.$.layer.checkControlMinWidth();
  }

  _maxItemsShown(newValue) {
    var total = newValue * 3 - 1 + 3; // 3rem for the layer paddings, 1rem for the margin bottom 0 of last item
    this.updateStyles({
      '--cells-dropdown-layer-layer-max-height': total + 'rem',
    });
  }

  _arrowMove(keyCode) {
    var items = this.$.layer.querySelectorAll('.item');
    var selectedIndex = Array.prototype.indexOf.call(items, this.$.layer.querySelector('.item.selected'));
    var target;
    if (keyCode === 37 || keyCode === 38) {
      target = selectedIndex === 0 ? items.length - 1 : selectedIndex - 1;
    } else {
      target = selectedIndex === items.length - 1 ? 0 : selectedIndex + 1;
    }
    this._select(items[target]);
  }

  _select(item) {
    this._clearSelected();
    item.focus();
    item.setAttribute('tabindex', 0);
    item.classList.add('selected');
  }

  _clearSelected() {
    var selected = this.$.layer.querySelector('.item.selected');
    if (selected) {
      selected.setAttribute('tabindex', -1);
      selected.classList.remove('selected');
    }
  }

  _itemClick(ev) {
    var item = ev.composedPath().find(function(elem) {
      return elem.classList.contains('item');
    });
    this._checkItem(item);
    this.close();
    this._restoreFocus();
  }

  _checkItem(item) {
    var index = this.items.indexOf(this.$.dr.itemForElement(item));
    this.selected = index;
    this.error = this.$.dr.itemForElement(item).error;
    this.dispatchEvent(new CustomEvent('user-selected-changed', {
      bubbles: true,
      composed: true,
      detail: this.$.dr.itemForElement(item).value,
    }));
  }
  /**
   * Fired when user checks a value in the layer
   * @event cells-dropdown-menu-user-value-changed
   */

  _opened(newValue) {
    if (!newValue) {
      this._clearSelected();
      if (this.required && !this.value) {
        this.$.layer.error = this.requiredMsg;
      } else if (this.error) {
        this.$.layer.error = this.error;
      } else {
        this.$.layer.error = '';
      }
    }
  }

  _setLabel(placeholder, _label) {
    return !_label ? placeholder : _label;
  }

  _restoreFocus() {
    setTimeout(function() {
      this.$.layer.focus();
    }.bind(this), 1);
  }

  _keyHandler(e) {
    var keyCode = e.keyCode;

    if (keyCode >= 37 && keyCode <= 40) {
      e.preventDefault();
      this._arrowPressed(keyCode);
    }

    if (keyCode === 32 || keyCode === 13) {
      e.preventDefault();
      this._selectKeyPressed();
    }

    if (keyCode === 27) {
      if (this.opened) {
        this._restoreFocus();
      }
    }
  }

  _arrowPressed(keyCode) {
    var checked;
    var first;

    if (!this.opened) {
      this.open();
      checked = this.$.layer.querySelector('.item[aria-checked="true"]');
      if (checked) {
        this._select(checked);
      } else {
        first = this.$.layer.querySelector('.item');
        this._select(first);
      }
    } else {
      var selected = this.$.layer.querySelector('.item.selected');
      if (!selected) {
        checked = this.$.layer.querySelector('.item[aria-checked="true"]');
        if (checked) {
          this._select(checked);
        } else {
          first = this.$.layer.querySelector('.item');
          this._select(first);
        }
      }
      this._arrowMove(keyCode);
    }
  }

  _selectKeyPressed() {
    if (!this.opened) {
      this.open();
      var checked = this.$.layer.querySelector('.item[aria-checked="true"]');
      if (checked) {
        this._select(checked);
      } else {
        var first = this.$.layer.querySelector('.item');
        this._select(first);
      }
    } else {
      var selected = this.$.layer.querySelector('.item.selected');
      if (selected) {
        this._checkItem(selected);
      }
      this.close();
      this._restoreFocus();
    }
  }

  _selectedObserver(newValue) {
    if (newValue > -1 && newValue <= this.items.length - 1 && this.items && this.items.length) {
      Polymer.flush();
      var elems = this.$.layer.querySelectorAll('.item');
      elems.forEach(function(elem, index) {
        elem.setAttribute('aria-checked', false);
      });
      elems[newValue].setAttribute('aria-checked', true);
      this._label = this.items[newValue].name;
      this.value = this.items[newValue].value;
    }
  }

  _someError(items) {
    if (items) {
      this.hasError = !!items.find(elem => elem.error);
    }
  }

  _itemsObserver(newValue, oldValue) {
    if ((!oldValue || !oldValue.length) && newValue && this.selected > -1) {
      this._selectedObserver(this.selected);
    }
    this._someError(newValue);
  }

  _valueChange(val) {
    this.dispatchEvent(new CustomEvent('cells-dropdown-menu-value', {
      bubbles: true,
      composed: true,
      detail: val,
    }));
  }
  /**
   * Fired when value changes
   * @event cells-dropdown-menu-value
   */

  _setClass() {
    return 'layer ' + this.classList.value;
  }

  _setErrorClass(error) {
    return error ? 'error' : '';
  }
}

customElements.define(cellsDropdownMenu.is, cellsDropdownMenu);
