(function() {

  'use strict';

  Polymer({

    is: 'cells-switch',

    hostAttributes: {
      tabindex: 0,
      'role': 'switch'
    },

    behaviors: [
      Polymer.IronCheckedElementBehavior,
      Polymer.IronA11yKeysBehavior
    ],

    properties: {
      /**
       * ID of icon to display in the button that toggles.
       */
      iconOn: {
        type: String,
        value: ''
      },

      /**
       * ID of icon to display in the button that toggles.
       */
      iconOff: {
        type: String,
        value: ''
      },

      /**
       * Determines the size of the toggle icon.
       */
      iconSize: {
        type: Number,
        value: 16
      },

      /**
       * Set to true, when you are using icons
       */
      withIcons: {
        type: Boolean,
        reflectToAttribute: true,
        value: false
      },

      /**
       * Set to true, when you are using a dark background style
       */
      darkBg: {
        type: Boolean,
        value: false
      },

      /**
       * Set to true to disable the switch
       */
      disabled: {
        type: Boolean,
        reflectToAttribute: true,
        notify: true,
        observer: '_disabled'
      },

      _icon: {
        type: String,
        computed: '_computeIcon(checked, withIcons)'
      },

      _target: {
        type: Object,
        value: function() {
          return this;
        }
      }
    },

    keyBindings: {
      'space': '_onSpaceKeydown'
    },

    listeners: {
      click: 'toggle'
    },

    _computeIcon: function(checked) {
      return (checked) ? this.iconOn : this.iconOff;
    },

    _computeClass: function(checked) {
      return (checked) ? 'on' : '';
    },

    _disabled: function(disabled) {
      this.setAttribute('aria-disabled', String(disabled));
      var tabindex = disabled ? -1 : 0;
      this.setAttribute('tabindex', tabindex);
    },

    _onSpaceKeydown: function(e) {
      if (e.detail.keyboardEvent.keyCode === 32) {
        e.preventDefault();
      }
      if (!this.disabled) {
        this.toggle();
      }
    },
    /**
     * Toggles button state position and `checked` property
     *  true if component is checked and false if is unchecked
     */
    toggle: function() {
      this.checked = !this.checked;
    },
    /**
     * Overriding 'checked' observer from Polymer.IronCheckedElementBehavior;
     * on change, it toggles `.on` class of `#handler` node
     * @param {Boolean} checked value
     */
    _checkedChanged: function(value) {
      this.active = value;
      this.dispatchEvent(new CustomEvent('iron-change', {
        bubbles: true,
        composed: true
      }));

      this.setAttribute('aria-label', this.getEffectiveTextContent());
      this.setAttribute('aria-checked', value);
      /**
       * Fired when `checked` value is changed,
       * with current status as payload
       *
       * @event cells-switch-changed
       */
      this.dispatchEvent(new CustomEvent('cells-switch-changed', {
        bubbles: true,
        composed: true,
        detail: value
      }));
    },
  });
}());
