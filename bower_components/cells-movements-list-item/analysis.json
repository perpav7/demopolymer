{
  "schema_version": "1.0.0",
  "elements": [
    {
      "description": "`<cells-movements-list-item>` displays some information about a card or account movement:\n\n  - movement amount\n  - remaining amount after the movement\n  - date of the operation\n  - expense category\n  - available options for the movement\n\nExample:\n\n```html\n<cells-movements-list-item\n  date=\"2018-02-21T00:00:00.000+0100\"\n  label=\"Cafetería\"\n  description=\"Pago con tarjeta\"\n  parsed-amount=\"-1\"\n  parsed-accounting-balance=\"13199.68\"\n  category=\"9999\"\n  category-description=\"Pendiente de categorizar\">\n</cells-movements-list-item>\n```\n\n## Styling\n\nThe following custom properties and mixins are available for styling:\n\n| Custom Property                                         | Description                                                        | Default                          |\n| :------------------------------------------------------ | :----------------------------------------------------------------- | :------------------------------- |\n| --cells-movements-list-item-border-bottom-color         | border-bottom color                                                | var(--bbva-100, #F4F4F4)         |\n| --cells-movements-list-item                             | empty mixin applied to the :host                                   |                                  |\n| --cells-movements-list-item-heading-color               | label color                                                        | var(--bbva-medium-blue, #237ABA) |\n| --cells-movements-list-item-heading                     | empty mixin applied to the label                                   |                                  |\n| --cells-movements-list-item-description-color           | description color                                                  | var(--bbva-500, #666666)         |\n| --cells-movements-list-item-description                 | empty mixin applied to descripton                                  |                                  |\n| --cells-movements-list-item-category-description        | empty mixin applied to category description                        |                                  |\n| --cells-movements-list-item-additional-info             | empty mixin applied to additional info (remaining amount + badges) |                                  |\n| --cells-movements-list-item-category-icon               | empty mixin applied to category icon                               |                                  |\n| --cells-movements-list-item-amount-color                | amount color                                                       | var(--bbva-500, #666666)         |\n| --cells-movements-list-item-amount                      | empty mixin applied to amount                                      |                                  |\n| --cells-movements-list-item-amount-integer              | empty mixin applied to the integer part of the amount              |                                  |\n| --cells-movements-list-item-amount-fractional           | empty mixin applied to the fractional part of the amount           |                                  |\n| --cells-movements-list-item-amount-currency             | empty mixin applied to the amount currency                         |                                  |\n| --cells-movements-list-item-amount-large-font-size      | font-size of the movement amount                                   | 1.125rem                         |\n| --cells-movements-list-item-amount-large                | empty mixin applied to the movement amount                         |                                  |\n| --cells-movements-list-item-amount-remaining-font-size  | font-size of the remaining amount                                  | 1rem                             |\n| --cells-movements-list-item-amount-remaining            | empty mixin applied to the remaining amount                        |                                  |\n| --cells-movements-list-item-remaining-amount-fractional | empty mixin applied to the fractional part of the remaining amount |                                  |\n| --cells-movements-list-item-remaining-amount-currency   | empty mixin applied to the currency of the remaining amount        |                                  |\n| --cells-movements-list-item-amount-negative-color       | color for negative amounts                                         | var(--bbva-dark-red, #B92A45)    |",
      "summary": "",
      "path": "cells-movements-list-item.js",
      "properties": [
        {
          "name": "date",
          "type": "string",
          "description": "Movement date.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 14,
              "column": 6
            },
            "end": {
              "line": 16,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          }
        },
        {
          "name": "dateFormat",
          "type": "string",
          "description": "Format for movement date.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 24,
              "column": 6
            },
            "end": {
              "line": 27,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "\"DD/MM/YYYY\""
        },
        {
          "name": "showCurrentYear",
          "type": "boolean",
          "description": "Attribute that marks if the current year in the movement date must be shown.\nOperations made in the current year don't display the year by default.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 36,
              "column": 6
            },
            "end": {
              "line": 39,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "false"
        },
        {
          "name": "label",
          "type": "string",
          "description": "Movement title.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 47,
              "column": 6
            },
            "end": {
              "line": 49,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          }
        },
        {
          "name": "description",
          "type": "string",
          "description": "Movement description.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 57,
              "column": 6
            },
            "end": {
              "line": 60,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "null"
        },
        {
          "name": "parsedAmount",
          "type": "Object",
          "description": "Parsed movement amount.\n\nFormat:\n```js\n{\n  value: 2.5,\n  currency: 'EUR'\n}\n```",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 77,
              "column": 6
            },
            "end": {
              "line": 82,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "{}"
        },
        {
          "name": "currencyCode",
          "type": "string",
          "description": "ISO 4217 for the currency.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 90,
              "column": 6
            },
            "end": {
              "line": 93,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "\"EUR\""
        },
        {
          "name": "localCurrency",
          "type": "string",
          "description": "ISO 4217 for the local currency.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 101,
              "column": 6
            },
            "end": {
              "line": 104,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "\"EUR\""
        },
        {
          "name": "language",
          "type": "string",
          "description": "Current language used to show different punctuation in amounts.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 112,
              "column": 6
            },
            "end": {
              "line": 115,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "\"es\""
        },
        {
          "name": "badges",
          "type": "Array",
          "description": "Movement options (badges).\n\nBadge structure:\n```js\n{\n extraClass: '',\n status: 'error' || 'success' || 'default',\n label: ''\n}\n```",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 132,
              "column": 6
            },
            "end": {
              "line": 137,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "[]"
        },
        {
          "name": "parsedAccountingBalance",
          "type": "Object",
          "description": "Parsed accounting balance.\nRemaining amount after the current movement.\n\nFormat:\n```js\n{\n  value: 2.5,\n  currency: 'EUR'\n}\n```",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 155,
              "column": 6
            },
            "end": {
              "line": 160,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "{}"
        },
        {
          "name": "category",
          "type": "number",
          "description": "Movement category.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 168,
              "column": 6
            },
            "end": {
              "line": 170,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          }
        },
        {
          "name": "categoryIconSize",
          "type": "number",
          "description": "Category icon size.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 178,
              "column": 6
            },
            "end": {
              "line": 181,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "15"
        },
        {
          "name": "categoryDescription",
          "type": "string",
          "description": "Category description.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 189,
              "column": 6
            },
            "end": {
              "line": 191,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          }
        },
        {
          "name": "hideCategory",
          "type": "boolean",
          "description": "Attribute that indicates if movement category must be hidden.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 199,
              "column": 6
            },
            "end": {
              "line": 202,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {}
          },
          "defaultValue": "false"
        }
      ],
      "methods": [],
      "staticMethods": [],
      "demos": [
        {
          "url": "demo/index.html",
          "description": ""
        }
      ],
      "metadata": {},
      "sourceRange": {
        "start": {
          "line": 1,
          "column": 0
        },
        "end": {
          "line": 205,
          "column": 1
        }
      },
      "privacy": "public",
      "superclass": "HTMLElement",
      "name": "CellsMovementsListItem",
      "attributes": [
        {
          "name": "date",
          "description": "Movement date.",
          "sourceRange": {
            "start": {
              "line": 14,
              "column": 6
            },
            "end": {
              "line": 16,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string"
        },
        {
          "name": "date-format",
          "description": "Format for movement date.",
          "sourceRange": {
            "start": {
              "line": 24,
              "column": 6
            },
            "end": {
              "line": 27,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string"
        },
        {
          "name": "show-current-year",
          "description": "Attribute that marks if the current year in the movement date must be shown.\nOperations made in the current year don't display the year by default.",
          "sourceRange": {
            "start": {
              "line": 36,
              "column": 6
            },
            "end": {
              "line": 39,
              "column": 7
            }
          },
          "metadata": {},
          "type": "boolean"
        },
        {
          "name": "label",
          "description": "Movement title.",
          "sourceRange": {
            "start": {
              "line": 47,
              "column": 6
            },
            "end": {
              "line": 49,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string"
        },
        {
          "name": "description",
          "description": "Movement description.",
          "sourceRange": {
            "start": {
              "line": 57,
              "column": 6
            },
            "end": {
              "line": 60,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string"
        },
        {
          "name": "parsed-amount",
          "description": "Parsed movement amount.\n\nFormat:\n```js\n{\n  value: 2.5,\n  currency: 'EUR'\n}\n```",
          "sourceRange": {
            "start": {
              "line": 77,
              "column": 6
            },
            "end": {
              "line": 82,
              "column": 7
            }
          },
          "metadata": {},
          "type": "Object"
        },
        {
          "name": "currency-code",
          "description": "ISO 4217 for the currency.",
          "sourceRange": {
            "start": {
              "line": 90,
              "column": 6
            },
            "end": {
              "line": 93,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string"
        },
        {
          "name": "local-currency",
          "description": "ISO 4217 for the local currency.",
          "sourceRange": {
            "start": {
              "line": 101,
              "column": 6
            },
            "end": {
              "line": 104,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string"
        },
        {
          "name": "language",
          "description": "Current language used to show different punctuation in amounts.",
          "sourceRange": {
            "start": {
              "line": 112,
              "column": 6
            },
            "end": {
              "line": 115,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string"
        },
        {
          "name": "badges",
          "description": "Movement options (badges).\n\nBadge structure:\n```js\n{\n extraClass: '',\n status: 'error' || 'success' || 'default',\n label: ''\n}\n```",
          "sourceRange": {
            "start": {
              "line": 132,
              "column": 6
            },
            "end": {
              "line": 137,
              "column": 7
            }
          },
          "metadata": {},
          "type": "Array"
        },
        {
          "name": "parsed-accounting-balance",
          "description": "Parsed accounting balance.\nRemaining amount after the current movement.\n\nFormat:\n```js\n{\n  value: 2.5,\n  currency: 'EUR'\n}\n```",
          "sourceRange": {
            "start": {
              "line": 155,
              "column": 6
            },
            "end": {
              "line": 160,
              "column": 7
            }
          },
          "metadata": {},
          "type": "Object"
        },
        {
          "name": "category",
          "description": "Movement category.",
          "sourceRange": {
            "start": {
              "line": 168,
              "column": 6
            },
            "end": {
              "line": 170,
              "column": 7
            }
          },
          "metadata": {},
          "type": "number"
        },
        {
          "name": "category-icon-size",
          "description": "Category icon size.",
          "sourceRange": {
            "start": {
              "line": 178,
              "column": 6
            },
            "end": {
              "line": 181,
              "column": 7
            }
          },
          "metadata": {},
          "type": "number"
        },
        {
          "name": "category-description",
          "description": "Category description.",
          "sourceRange": {
            "start": {
              "line": 189,
              "column": 6
            },
            "end": {
              "line": 191,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string"
        },
        {
          "name": "hide-category",
          "description": "Attribute that indicates if movement category must be hidden.",
          "sourceRange": {
            "start": {
              "line": 199,
              "column": 6
            },
            "end": {
              "line": 202,
              "column": 7
            }
          },
          "metadata": {},
          "type": "boolean"
        }
      ],
      "events": [],
      "styling": {
        "cssVariables": [],
        "selectors": []
      },
      "slots": [],
      "tagname": "cells-movements-list-item"
    }
  ]
}
