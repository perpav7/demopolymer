![cells-movements-list-item screenshot](cells-movements-list-item.png)

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)

[Demo of component in Cells Catalog](https://au-bbva-ether-cellscatalogs.appspot.com/?view=docs#/component/cells-movements-list-item)

# &lt;cells-movements-list-item&gt;

`<cells-movements-list-item>` displays some information about a card or account movement:

  - movement amount
  - remaining amount after the movement
  - date of the operation
  - expense category
  - available options for the movement

Example:

```html
<cells-movements-list-item
  date="2018-02-21T00:00:00.000+0100"
  label="Cafetería"
  description="Pago con tarjeta"
  parsed-amount="-1"
  parsed-accounting-balance="13199.68"
  category="9999"
  category-description="Pendiente de categorizar">
</cells-movements-list-item>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom Property                                         | Description                                                        | Default                          |
| :------------------------------------------------------ | :----------------------------------------------------------------- | :------------------------------- |
| --cells-movements-list-item-border-bottom-color         | border-bottom color                                                | var(--bbva-100, #F4F4F4)         |
| --cells-movements-list-item                             | empty mixin applied to the :host                                   |                                  |
| --cells-movements-list-item-heading-color               | label color                                                        | var(--bbva-medium-blue, #237ABA) |
| --cells-movements-list-item-heading                     | empty mixin applied to the label                                   |                                  |
| --cells-movements-list-item-description-color           | description color                                                  | var(--bbva-500, #666666)         |
| --cells-movements-list-item-description                 | empty mixin applied to descripton                                  |                                  |
| --cells-movements-list-item-category-description        | empty mixin applied to category description                        |                                  |
| --cells-movements-list-item-additional-info             | empty mixin applied to additional info (remaining amount + badges) |                                  |
| --cells-movements-list-item-category-icon               | empty mixin applied to category icon                               |                                  |
| --cells-movements-list-item-amount-color                | amount color                                                       | var(--bbva-500, #666666)         |
| --cells-movements-list-item-amount                      | empty mixin applied to amount                                      |                                  |
| --cells-movements-list-item-amount-integer              | empty mixin applied to the integer part of the amount              |                                  |
| --cells-movements-list-item-amount-fractional           | empty mixin applied to the fractional part of the amount           |                                  |
| --cells-movements-list-item-amount-currency             | empty mixin applied to the amount currency                         |                                  |
| --cells-movements-list-item-amount-large-font-size      | font-size of the movement amount                                   | 1.125rem                         |
| --cells-movements-list-item-amount-large                | empty mixin applied to the movement amount                         |                                  |
| --cells-movements-list-item-amount-remaining-font-size  | font-size of the remaining amount                                  | 1rem                             |
| --cells-movements-list-item-amount-remaining            | empty mixin applied to the remaining amount                        |                                  |
| --cells-movements-list-item-remaining-amount-fractional | empty mixin applied to the fractional part of the remaining amount |                                  |
| --cells-movements-list-item-remaining-amount-currency   | empty mixin applied to the currency of the remaining amount        |                                  |
| --cells-movements-list-item-amount-negative-color       | color for negative amounts                                         | var(--bbva-dark-red, #B92A45)    |
