/**
 * @customElement
 * @summary Exposes the HTML of component demo cases for cells-demo-helper
 * @polymer
 * @extends {Polymer.Element}
 */
class cellsDemoCase extends Polymer.Element {
  static get is() {
    return 'cells-demo-case';
  }

  static get properties() {
    return {
      /**
       * Stores the component content's HTML as a string
       */
      inner: {
        type: String
      },

      /**
       * Title of the demo case
       */
      heading: {
        type: String
      },

      /**
       * Description of the demo case
       */
      description: {
        type: String
      }
    }
  }

  get inner() {
    return this.querySelectorAll('template')[0].innerHTML;
  }

  /**
   * Returns the current innerHTML of the template tag in the content
   */
  getInner() {
    return this.inner;
  }

}

customElements.define(cellsDemoCase.is, cellsDemoCase);
