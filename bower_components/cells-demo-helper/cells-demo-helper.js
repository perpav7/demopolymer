/**
 * @customElement
 * @summary Showcases components demo cases, with multiple options such as i18n or resizable viewport
 * @polymer
 * @demo demo/index.html
 * @extends {Polymer.Element}
 */
class cellsDemoHelper extends Polymer.Element {
  static get is() {
    return 'cells-demo-helper';
  }

  static get properties() {
    return {
      /**
       * Array with the demo cases
       */
      _cases: {
        type: Array
      },

      /**
       * Current markdown code to show in code snippet bar
       */
      _markdown: {
        type: String,
        value: ''
      },

      /**
       * Current case title
       */
      _caseTitle: {
        type: String,
        value: ''
      },

      /**
       * Current case description
       */
      _description: {
        type: String,
        value: ''
      },

      /**
       * Whether the demo must show the lang switcher
       */
      i18n: {
        type: Boolean,
        value: false
      },

      /**
       * Array of custom langs to pass to cells-lang-demo-switcher
       */
      langs: {
        type: Array
      },

      /**
       * Current selected lang
       */
      _lang: {
        type: String,
        value: 'en',
        notify: true,
        observer: '_setIframeLang'
      },

      /**
       * Path of custom locales folder to pass to cells-lang-demo-switcher
       */
      localesPath: {
        type: String
      },

      /**
       * Array of events which cells-demo-event-toaster should listen for
       */
      events: {
        type: Array
      },

      /**
       * Default resolution key for the iframe
       */
      resolution: {
        type: String,
        value: 'mobile'
      },

      _resolution: {
        type: Object
      },

      /**
       * Custom breakpoints for cells-demo-resizable-viewport
       */
      breakpoints: {
        type: Object
      },

      /**
       * Default "no theme" option text
       */
      _noTheme: {
        type: String,
        value: 'standalone (no theme)'
      },

      /**
       * Additional available themes for the demo
       */
      availableThemes: {
        type: Array
      },

      /**
       * Full list of themes used
       */
      _themes: {
        type: Array,
        computed: '_computedThemes(_noTheme, availableThemes)'
      },

      /**
       * Selected theme on page load
       */
      defaultTheme: {
        type: String,
        value: ''
      },

      /**
       * Index of the selected theme
       */
      selectedTheme: {
        type: Number,
        value: 0,
        notify: true
      },

      /**
       * Stores current selected theme
       */
      _currentTheme: {
        type: String
      },

      /**
       * Stores current iframe template
       */
      _tpl: {
        type: String
      },

      /**
       * Index of the selected demo-case.
       */
      selected: {
        type: Number,
        value: 0,
        notify: true
      },

      /**
       * Prevents current document head to be passed on to the iframe. Useful for testing purposes
       */
      _noHead: {
        type: Boolean,
        value: false
      },

      /**
       * Allow to show multiple toasts simultaneously
       */
      multiToast: {
        type: Boolean,
        value: false
      },

      _toastAlign: {
        type: String,
        value: 'bottom'
      },

      _toastPositionTarget: {
        type: Element
      },

      _tabs: {
        type: Array,
        value: function() {
          return ['Preview', 'Code'];
        }
      },

      /**
       * Used to set a negative tabindex to focusable elements
       * when the code tab is active.
       */
      _tabindex: {
        type: Number
      },

      _selectedTab: {
        type: Number,
        value: 0,
        notify: true,
        observer: '_selectedTabChanged'
      },

      /**
       * Set to true to hide all the UI elements except the preview for development purposes.
       */
      hideUi: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      /**
       * Used to show the spinner only the first time (not between demos).
       */
      _firstRender: {
        type: Boolean,
        value: true
      },

      /**
       * Show console warnings about style modules not found in components (shared-styles).
       */
      showStyleModuleWarnings: {
        type: Boolean,
        value: false
      }
    };
  }

  static get observers() {
    return [
      '_onSelectedCaseIndexChanged(selected, _cases)',
      '_onSelectedThemeIndexChanged(selectedTheme, _cases, _themes)',
      '_onDefaultThemeChanged(defaultTheme, _themes)'
    ];
  }

  connectedCallback() {
    super.connectedCallback();
    setTimeout(() => {
      this._setCasesDropdown();
    }, 1000);

    // change the toast position and position target if the component is inside an iframe (Catalog App)
    if (window !== top) {
      this._toastAlign = 'top';
      this._toastPositionTarget = this.$.main;
    }
  }

  _onSelectedCaseIndexChanged(index, cases) {
    if (!cases) {
      return;
    }
    this._setIframe(cases[index]);
  }

  _onSelectedThemeIndexChanged(index, cases, themes) {
    if (!cases || !themes) {
      return;
    }
    this._currentTheme = themes[index].value;
    this._setIframe(cases[this.selected]);
  }

  _onDefaultThemeChanged(defaultTheme, themes) {
    if (!themes) {
      return;
    }

    var defaultThemeInThemes = themes.find(function(theme) {
      return theme.name === defaultTheme;
    });

    if (defaultThemeInThemes) {
      this.selectedTheme = themes.indexOf(defaultThemeInThemes);
    }
  }

  _isActiveTab(selectedTab, tabName) {
    return this._tabs[selectedTab] === tabName ? 'visible' : '';
  }

  _selectedTabChanged(tab) {
    // return false instead of zero to remove the attribute
    this._tabindex = tab === 1 ? -1 : false;
    var iframe = this.$.iframeContainer.querySelector('iframe');
    if (iframe) {
      iframe.setAttribute('tabindex', this._tabindex);
    }
  }

  /**
   * Creates array with full list of themes for themes dropdown
   */
  _computedThemes(theme, availableThemes) {
    if (!availableThemes) {
      return;
    }

    var group = availableThemes.slice();
    group.push(theme);

    return group.map(function(item) {
      return {
        name: item,
        value: item.trim().toLowerCase()
      };
    });
  }

  /**
   * Returns the script of initial i18n values to insert in the iframe
   */
  _langAdder() {
    var langSet = '<script>'
      + 'window.I18nMsg = {};'
      + 'window.I18nMsg.lang = "' + this._lang + '";'
      + 'window.I18nMsg.url = "' + this.localesPath + '";'
      + '</script>';
    return langSet;
  }

  /**
   * Changes dinamically the lang attribute and i18n variables in the iframe
   * @param { String } lng lang code
   */
  _setIframeLang(lng) {
    if (this.i18n) {
      window.I18nMsg = window.I18nMsg || {};
    }
    var iframe = this.$.iframeContainer.querySelector('iframe');
    var localesPath = this.i18n ? window.I18nMsg.url : false;
    if (iframe) {
      iframe.contentDocument.querySelector('html').setAttribute('lang', lng);
      if (iframe.contentWindow.I18nMsg && localesPath) {
        iframe.contentWindow.I18nMsg.lang = lng;
        iframe.contentWindow.I18nMsg.url = localesPath;
      }
    }
  }

  /**
   * Populates cases array based on available cells-demo-case contents
   */
  _setCasesDropdown() {
    var shadyChildren = this.querySelectorAll('cells-demo-case');
    var result = [].map.call(shadyChildren, function(item) {
      return {
        name: item.heading,
        value: item
      };
    });

    this._cases = result;
  }

  _getHeadForIframe() {
    var head = document.querySelector('head');
    var clone = head.cloneNode(true);

    var scripts = clone.querySelectorAll('script');
    for (var i = 0, len = scripts.length; i < len; i++) {
      if (scripts[i].src.indexOf('webcomponents') > -1) {
        var preload = document.createElement('link');
        preload.href = scripts[i].src;
        preload.rel = 'preload';
        preload.as = 'script';
        scripts[i].parentNode.insertBefore(preload, scripts[i]);
        scripts[i].parentNode.removeChild(scripts[i]);
        break;
      }
    }

    var lazy = clone.querySelectorAll('link[rel="lazy-import"');
    /* istanbul ignore else */
    if (lazy.length) {
      [].forEach.call(clone.querySelectorAll('link[rel="import"]'), function(imp) {
        clone.removeChild(imp);
      });
      [].forEach.call(lazy, function(imp) {
        imp.rel = 'import';
      });
    }

    [].forEach.call(clone.querySelectorAll('link[rel="import"]'), function(imp) {
      if (imp.getAttribute('href').indexOf('cells-demo-helper') > -1 || imp.hasAttribute('no-import')) {
        imp.rel = 'no-import';
      }
    });

    if (!this.showStyleModuleWarnings) {
      var firstImport = clone.querySelector('link[rel="import"]');
      var logUtilsImport = document.createElement('link');
      logUtilsImport.rel = 'import';
      logUtilsImport.href = '../../cells-log-utils/cells-log-utils.html';
      firstImport.parentNode.insertBefore(logUtilsImport, firstImport);
    }

    return clone.innerHTML;
  }

  /**
   * Generates iframe for selected case
   */
  _setIframe(elem) {
    this._cleanIframe();
    this._generateMarkdown(elem.value);

    var iframe = this._generateIframe(elem);
    this.$.iframeContainer.appendChild(iframe);

    // prevent accessing to document not created yet
    /* istanbul ignore else */
    if (!iframe.contentDocument) {
      return;
    }

    iframe.contentDocument.open();
    iframe.contentDocument.write(this._tpl);
    iframe.contentDocument.close();

    var onWebComponentsReady = function() {
      iframe.contentDocument.body.removeAttribute('unresolved');
      this._passEvents(iframe);
      if (this._firstRender) {
        this._firstRender = false;
        this.dispatchEvent(new CustomEvent('demo-loaded-complete', {
          bubbles: true,
          composed: true
        }));
      }
    };
    iframe.contentDocument.addEventListener('WebComponentsReady', onWebComponentsReady.bind(this));
  }

  _generateIframe(elem) {
    this._iframeHead = this._iframeHead || this._getHeadForIframe();
    var head = this._noHead ? '' : /* istanbul ignore next */ this._iframeHead;

    var iframe = document.createElement('iframe');
    var langScript = this.i18n ? this._langAdder() : '';
    var themeImport = '';
    var themeName = '';
    var caseNameRaw = elem.name;
    var caseName = caseNameRaw.replace(/[^a-zA-Z0-9 ]/g, '').replace(/\s+/g, '-').toLowerCase();
    var headTag = langScript + head;

    iframe.addEventListener('load', function onLoad() {
      onLoad.webComponentsReadyLoaded = onLoad.webComponentsReadyLoaded || false;
      if (!onLoad.webComponentsReadyLoaded) {
        onLoad.webComponentsReadyLoaded = true;
        return;
      }

      // for testing purposes
      this.dispatchEvent(new CustomEvent('iframe-loaded', {
        bubbles: true,
        composed: true
      }));

    }.bind(this));

    if (this._currentTheme && this._currentTheme !== this._noTheme) {
      themeImport = '<link rel="import" href="../../' + this._currentTheme + '/' + this._currentTheme + '.html">';
      headTag = langScript + themeImport + head;
      themeName = this._currentTheme;
    }

    var classes = [caseName, themeName].join(' ');
    var bodyTag = '<body unresolved ontouchstart id="iframeBody" class="' + classes + '" onload="' +
      'var d = document; d.getElementsByTagName(\'head\')[0].' +
      'appendChild(d.createElement(\'script\')).src' +
      '=\'../../webcomponentsjs/webcomponents-lite.js\'"">';

    var html = [
      '<html lang="' + this._lang + '">',
      '<head>' + headTag + '</head>',
      bodyTag,
      this._parseContent(elem.value.getInner()),
      '</body></html>'
    ].join('');

    this._tpl = html;
    return iframe;
  }

  /**
   * Removes existing iframes
   */
  _cleanIframe() {
    var old = this.$.iframeContainer.querySelector('iframe');
    if (old) {
      this.$.iframeContainer.removeChild(old);
      old = null;
    }
  }

  _parseContent(str) {
    return str
      .replace(/=""/g, '')
      .replace(/="{/g, '=\'{')
      .replace(/}"/g, '}\'')
      .replace(/="\[/g, '=\'[')
      .replace(/\]"/g, ']\'')
      .replace(/&quot;/g, '"')
      .replace(/='\[\[/g, '="[[')
      .replace(/\]\]'/g, ']]"')
      .replace(/='\{\{/g, '="{{')
      .replace(/\}\}'/g, '}}"');
  }

  /**
   * Generates markdown code from the case
   */
  _generateMarkdown(item) {
    var template = this._parseContent(item.getInner());
    if (!template) {
      this._markdown = '```\n```';
      return;
    }
    this._caseTitle = item.heading;
    this._description = item.description;
    var snippet = this.$.marked.unindent(template);
    this._markdown = '```\n' + snippet + '\n' + '```';
  }

  /**
   * Creates listener for events inside iframe and propagates events firing to cells-demo-event-toaster
   */
  _passEvents(iframe) {
    if (this.events) {
      var eventToaster = this.shadowRoot.querySelector('#eventToaster');
      var events = this.events;
      iframe.contentDocument.eventParentDispatcher = function(name) {
        iframe.contentDocument.addEventListener(name, function(e) {
          var ev = new CustomEvent(name, e);
          eventToaster.dispatchEvent(ev);
        });
      };
      for (var i = 0; i < events.length; i++) {
        iframe.contentDocument.eventParentDispatcher(events[i]);
      }
    }
  }

  /**
   * Copies code snippet to clipboard
   */
  _copyToClipboard() {
    var snipRange = document.createRange();
    snipRange.selectNodeContents(this.$.code);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(snipRange);
    var result = false;
    try {
      result = document.execCommand('copy');
      this.$.copyButton.innerText = 'Copied!';
    } catch (err) {
      /* istanbul ignore next */ Polymer.Base._error(err);
      /* istanbul ignore next */ this.$.copyButton.innerText = 'Error';
    }
    setTimeout(this._resetCopyButtonState.bind(this), 1000);
    selection.removeAllRanges();
    return result;
  }

  /**
   * Reset copy button text
   */
  _resetCopyButtonState() {
    this.$.copyButton.innerText = 'Copy';
  }

  _computeHeight(height) {
    return height !== undefined ? height : 640;
  }
}

customElements.define(cellsDemoHelper.is, cellsDemoHelper);
