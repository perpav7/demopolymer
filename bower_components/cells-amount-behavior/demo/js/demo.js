/* eslint-disable */
var demoComp;
var mocksAmount = {
  simple: {
    amount: -27000009.045
  },
  zero: {
    amount: 0
  },
  integer: {
    amount: 1500
  },
  longInteger: {
    amount: 10000000000
  }
};
var mocksCurrency = {
  usa: {
    currency: 'USD'
  },
  ars: {
    currency: 'ARS'
  }
};

document.addEventListener('WebComponentsReady', function() {
  demoComp = Polymer.dom(this.root).querySelector('demo-component')
  demoComp.set('number', mocksAmount['simple'].amount);
  demoComp.set('amount', mocksAmount['simple'].amount);
  demoComp.set('longinteger', mocksAmount['longInteger'].amount);
});
