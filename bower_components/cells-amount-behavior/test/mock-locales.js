var localesES = {
  "simple-element-info": {
    "message": "Informacion"
  },
  "simple-element-cancel": {
    "message": "Cancelar"
  },
  "simple-element-warn": {
    "message": "Advertir"
  }
};
var localesEN = {
  "simple-element-info": {
    "message": "Information"
  },
  "simple-element-cancel": {
    "message": "Cancel"
  },
  "simple-element-warn": {
    "message": "Warn"
  }
};
