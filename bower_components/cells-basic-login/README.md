![cells-basic-login screenshot](cells-basic-login.png)

# cells-basic-login

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-basic-login)

`<cells-basic-login>` is a login form with __forgotten password__ and __remind user__ possibilities.

Basic example:
```html
<cells-basic-login icon-checked="coronita:checkmark" icon-unchecked="coronita:close"
  icon-toggle="coronita:menu"></cells-basic-login>
```

Login with saved user example:
```html
<cells-basic-login icon-checked="coronita:checkmark" icon-unchecked="coronita:close"
  icon-toggle="coronita:menu" name="Francisco" saved-user user-reminded></cells-basic-login>
```

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                                 | Selector                                     | CSS Property     | Value                                                             |
| ----------------------------------------------- | -------------------------------------------- | ---------------- | ----------------------------------------------------------------- |
| --cells-basic-login-inputs-icon-color           | :host > --cells-molecule-input-icon-visible: | color            |  ![#006EC1](https://placehold.it/15/006EC1/000000?text=+) #006EC1 |
| --cells-fontDefault                             | :host                                        | font-family      |  sans-serif                                                       |
| --cells-basic-login-bg-color                    | :host                                        | background-color |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff          |
| --cells-basic-login-welcome-border-bottom-color | .welcome                                     | border-bottom    |  ![#E5E5E5](https://placehold.it/15/E5E5E5/000000?text=+) #E5E5E5 |
| --cells-basic-login-form-border-bottom-color    | .form                                        | border-bottom    |  ![#E5E5E5](https://placehold.it/15/E5E5E5/000000?text=+) #E5E5E5 |
| --cells-basic-login-remember-label-color        | .remember .label                             | color            |  ![#717780](https://placehold.it/15/717780/000000?text=+) #717780 |
| --cells-basic-login-links-color                 | .link                                        | color            |  ![#006EC1](https://placehold.it/15/006EC1/000000?text=+) #006EC1 |
### @apply
| Mixins                                      | Selector                                                | Value |
| ------------------------------------------- | ------------------------------------------------------- | ----- |
| --cells-basic-login-input-wrapper           | :host > --cells-molecule-input-wrapper:                 | {}    |
| --cells-basic-login-withcontent-field-label | :host > --cells-molecule-input-withcontent-field-label: | {}    |
| --cells-basic-login-withcontent-field-input | :host > --cells-molecule-input-withcontent-field-input: | {}    |
| --cells-basic-login-inputs                  | :host > --cells-molecule-input:                         | {}    |
| --cells-basic-login-inputs-icons            | :host > --cells-molecule-input-icon-visible:            | {}    |
| --cells-basic-login                         | :host                                                   | {}    |
| --cells-basic-login-inputs                  | cells-molecule-input                                    | {}    |
| --cells-basic-login-inputs-active           | cells-molecule-input:active                             | {}    |
| --cells-basic-login-welcome                 | .welcome                                                | {}    |
| --cells-basic-login-user-msg                | .user-msg                                               | {}    |
| --cells-basic-login-user-greeting           | .user-greeting                                          | {}    |
| --cells-basic-login-user-name               | .user-name                                              | {}    |
| --cells-basic-login-form                    | .form                                                   | {}    |
| --cells-basic-login-button                  | .form .btn-login                                        | {}    |
| --cells-basic-login-remember                | .remember                                               | {}    |
| --cells-basic-login-remember-cells-switch   | .remember cells-switch                                  | {}    |
| --cells-basic-login-remember-first-child    | .remember div:first-child                               | {}    |
| --cells-basic-login-remember-label          | .remember .label                                        | {}    |
| --cells-basic-login-links                   | .link                                                   | {}    |
| --cells-basic-login-forgot-pwd              | .forgot-pwd                                             | {}    |
| --cells-basic-login-stripes-bar             | .stripes-bar                                            | {}    |
| --cells-basic-login-layout-row-center       | .layout-row-center                                      | {}    |

## Public methods

* reset(): Resets login form

## Events

* `cells-basic-login-success`: Fires an event after the form has been validated with the form data (user, password, reminded status)
* `cells-basic-login-error`: Fires an event if form is not correct
* `cells-basic-login-forgotten-password`: Fires an event on click of forgotten password
* `cells-basic-login-clear-reminded`: Fires an event if reminded user is cleared to intro another one
